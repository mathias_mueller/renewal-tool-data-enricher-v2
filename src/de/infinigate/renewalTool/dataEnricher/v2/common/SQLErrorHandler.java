package de.infinigate.renewalTool.dataEnricher.v2.common;

import java.sql.SQLException;

import javax.swing.JOptionPane;

/**
 * Class that handles SQLExceptions.
 * @author mathias.mueller
 */
public class SQLErrorHandler {
	
	/**
	 * Helper class, no instances needed.
	 */
	private SQLErrorHandler() {
		// not used.
	}
	
	/**
	 * Tries to give some user friendly explanation through known SQLStates and ErrorCodes before giving the Exception-Message.
	 * @param exception the SQLException to search for further hints on the cause
	 * @param firstLine the first line of the messageDialog that will be displayed to be able to further customize the dialog
	 */
	public static void makeSQLExceptionUserReadable(SQLException exception, String firstLine) {
		String userFriendlyExplanation = "";

		if (exception.getSQLState().equals("S0005") && exception.getErrorCode() == 229)
			userFriendlyExplanation = "You seem to lack some permissions on the used database tables.\nThe following text may help your admin in solving this issue, please send it to him:\n";
		else if (exception.getSQLState().equals("08S01"))
			userFriendlyExplanation = "There seems to be a problem with the setup of the tool.\nYour machine is not configured to authenticate at the SQL-server using "
					+ "\"integrated authentication\".\nPlease send a message with your problem to internal-support@infinigate.com to get help.\nMessage for internal-support:\n\n";
		else 
			userFriendlyExplanation = "An unknown database error occured.\nPlease provide your admin with the following text for further assistance:\n";
		
		JOptionPane.showMessageDialog(null, firstLine + "\n" + userFriendlyExplanation + exception.getMessage(), "SQL Error!", JOptionPane.ERROR_MESSAGE);		
	}

}
