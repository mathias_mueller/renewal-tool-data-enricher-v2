package de.infinigate.renewalTool.dataEnricher.v2.common;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

import javax.swing.JOptionPane;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

import de.infinigate.renewalTool.dataEnricher.v2.gui.Wizard;

/**
 * Class parsing a presented config-file in XML-format.
 * The configuration-file must be present within this application's execution path
 * Also holds the parsed information to access from various sources
 * @author mathias.mueller
 */
public class XmlConfigManager {

	/**
	 * Name of the mother-node of general config data.
	 */
	private static final String NAME_ROOT_GENERAL_CONFIG = "configData";

	/**
	 * Name of the child-node that contains the address of the sql-server.
	 */
	private static final String NAME_CHILD_SQL_ADDRESS = "sqladdress";
	
	/**
	 * Name of the child-node that contains the name of the NAV-DB.
	 */
	private static final String NAME_CHILD_NAVDBNAME = "navdbname";

	/**
	 * Name of the child-node that contains the name of the company in NAV.
	 */
	private static final String NAME_CHILD_NAVCOMPANYNAME = "navcompanyname";

	/**
	 * Name of the mother-node that contains user-specific configuration.
	 */
	private static final String NAME_ROOT_USER_CONFIG = "userConfig";

	/**
	 * Name of the child-node that contains the lastly used import profile.
	 */
	private static final String NAME_CHILD_IMPORTPROFILE = "importProfile";

	/**
	 * Name of the child-node that contains the path of the last file used.
	 */
	private static final String NAME_CHILD_FILEPATH = "filePath";
	
	/**
	 * Name of the file that shall be parsed.
	 */
	private static final String CONFIG_FILENAME = "enricherConfig.xml";
	
	/**
	 * The value of the "sqlAddress" XML-Field.
	 */
	private String sqlAddress;
	
	/**
	 * The value of the "navDbName" XML-Field.
	 */
	private String navDbName;
	
	/**
	 * The value of the "navCompanyName" XML-Field.
	 */
	private String navCompanyName;
	
	/**
	 * The value of the "importProfile" XML-field.
	 */
	private String importProfile;
	
	/**
	 * The value of the "filePath" XML-field.
	 */
	private String filePath; 

	/**
	 * The xmlFile as File object.
	 */
	private File xmlFile;
	
	/**
	 * Standard CTor.
	 */
	public XmlConfigManager() {
		parseXmlConfig();
	}
	
	/**
	 * Parses the xml config-file.
	 */
	private void parseXmlConfig() {
	
		SAXBuilder saxBuild = new SAXBuilder();
		String path = Wizard.class.getProtectionDomain().getCodeSource().getLocation().getPath();
		try {
			path = URLDecoder.decode(path, "UTF-8");
		} catch (UnsupportedEncodingException e1) {
			e1.printStackTrace();
		}
		path = path.substring(0, path.lastIndexOf('/') + 1);
		
		xmlFile = new File(path + CONFIG_FILENAME);
			
		try {
			Document doc = saxBuild.build(xmlFile);
			Element rootNode = doc.getRootElement();
			Element configData = rootNode.getChild(NAME_ROOT_GENERAL_CONFIG);
			
			sqlAddress = configData.getChildText(NAME_CHILD_SQL_ADDRESS);	
	
			if (getSqlAddress() == null) {
				JOptionPane.showMessageDialog(null, "An Error occured when trying to obtain the " +
						"sqlAddress from the Config-XML.", "Error reading config", JOptionPane.ERROR_MESSAGE);
				System.exit(1);
			}
			
			navDbName = configData.getChildText(NAME_CHILD_NAVDBNAME);	
			
			if (getSqlAddress() == null) {
				JOptionPane.showMessageDialog(null, "An Error occured when trying to obtain the " +
						"navDbName from the Config-XML.", "Error reading config", JOptionPane.ERROR_MESSAGE);
				System.exit(1);
			}
			
			navCompanyName = configData.getChildText(NAME_CHILD_NAVCOMPANYNAME);	
			
			if (getSqlAddress() == null) {
				JOptionPane.showMessageDialog(null, "An Error occured when trying to obtain the " +
						"navCompanyName from the Config-XML.", "Error reading config", JOptionPane.ERROR_MESSAGE);
				System.exit(1);
			}
			
			String userName = System.getProperty("user.name");
			
			Element userConfig = rootNode.getChild(NAME_ROOT_USER_CONFIG);
			if (userConfig != null) {
				Element userNameNode = userConfig.getChild(userName);
				if (userNameNode != null) {
					importProfile = userNameNode.getChildText(NAME_CHILD_IMPORTPROFILE);
					filePath = userNameNode.getChildText(NAME_CHILD_FILEPATH);
				}
			}
			
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "An error occured when trying to read the " +
					"necessary configuration data. Make sure a 'importerConfig.xml' is present in the .jars-folder.", 
					"Error Parsing XML", JOptionPane.ERROR_MESSAGE);
			System.exit(1);
		} 
	}

	/**
	 * @return the sqlAddress
	 */
	public String getSqlAddress() {
		return sqlAddress;
	}

	/**
	 * @return the navDbName
	 */
	public String getNavDbName() {
		return navDbName;
	}

	/**
	 * @return the navCompanyName
	 */
	public String getNavCompanyName() {
		return navCompanyName;
	}
	
	/** 
	 * Writes back the selected ImportProfile to the XML-File.
	 * @param importProfileToWrite the importProfile Code that should be written to the xmlConfig
	 * @param filePathToWrite the filePath to write back to the XML	
	 */
	public void writeBackUserConfig(String importProfileToWrite){
		
		SAXBuilder saxBuild = new SAXBuilder();
		try {
			Document doc = saxBuild.build(xmlFile);
			Element rootNode = doc.getRootElement();
			
			boolean userConfigNewlyCreated = false;
			
			Element userConfig = rootNode.getChild(NAME_ROOT_USER_CONFIG); 
			if (userConfig == null) {
				userConfig = new Element(NAME_ROOT_USER_CONFIG);
				userConfigNewlyCreated = true;
			}
			
			String userName = System.getProperty("user.name");
			
			boolean userNameNodeNewlyCreated = false;
			
			Element userNameNode = userConfig.getChild(userName);
			if (userNameNode == null) {
				userNameNode = new Element(userName);
				userNameNodeNewlyCreated = true;
			}
			
			Element importProfile = userNameNode.getChild(NAME_CHILD_IMPORTPROFILE);
			if (importProfile == null)
				userNameNode.addContent(new Element(NAME_CHILD_IMPORTPROFILE).setText(importProfileToWrite));
			else 
				importProfile.setText(importProfileToWrite);

			if (userNameNodeNewlyCreated)
				userConfig.addContent(userNameNode);
			
			if (userConfigNewlyCreated)
				rootNode.addContent(userConfig);
			
			FileWriter fw = new FileWriter(xmlFile);
			
			XMLOutputter xmlOut = new XMLOutputter(); 
			xmlOut.setFormat(Format.getPrettyFormat());
			xmlOut.output(doc, fw);
			
		} catch (JDOMException e) {
			JOptionPane.showMessageDialog(null, "\"JDOM\" error writing back the selected Import Profile to the configuration file.");
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null, "\"IO\" error writing back the selected Import Profile to the configuration file.");
		}
	}

	/**
	 * @return the importProfile
	 */
	public String getImportProfile() {
		return importProfile;
	}

	/**
	 * @return the filePath
	 */
	public String getFilePath() {
		return filePath;
	}
	
}
