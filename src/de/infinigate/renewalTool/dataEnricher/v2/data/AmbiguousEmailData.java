package de.infinigate.renewalTool.dataEnricher.v2.data;

public class AmbiguousEmailData {

	/**
	 * Whether the change set up in this object shall be applied.
	 */
	private boolean apply;
	
	/**
	 * The ResellerName from the RenewalData records. 
	 */
	private String resellerName;
	
	/**
	 * All different email-addresses that are present for the Reseller.
	 */
	private String[] emailsFromInput;
	
	/**
	 * The ResellerName that should be used when creating a ResellerRule;
	 */
	private String nameForRule;
	
	/**
	 * The email-address that should be used for the new ResellerRule.
	 */
	private String emailForRule;

	/**
	 * Standard CTor filling fields.
	 */
	public AmbiguousEmailData(boolean apply, String resellerName,
			String[] emailsFromInput, String nameForRule,
			String emailForRule) {

		this.apply = apply;
		this.resellerName = resellerName;
		this.emailsFromInput = emailsFromInput;
		this.nameForRule = nameForRule;
		this.emailForRule = emailForRule;
	}

	/*
	 * "Naked" CTor without any initializing.
	 */
	public AmbiguousEmailData() {
		
	}

	/**
	 * @return the apply
	 */
	public boolean isApply() {
		return apply;
	}

	/**
	 * @param apply the apply to set
	 */
	public void setApply(boolean apply) {
		this.apply = apply;
	}

	/**
	 * @return the resellerName
	 */
	public String getResellerName() {
		return resellerName;
	}

	/**
	 * @param resellerName the resellerName to set
	 */
	public void setResellerName(String resellerName) {
		this.resellerName = resellerName;
	}

	/**
	 * @return the emailsFromInput
	 */
	public String[] getEmailsFromInput() {
		return emailsFromInput;
	}

	/**
	 * @param emailsFromInput the emailsFromInput to set
	 */
	public void setEmailsFromInput(String[] emailsFromInput) {
		this.emailsFromInput = emailsFromInput;
	}

	/**
	 * @return the nameForRule
	 */
	public String getNameForRule() {
		return nameForRule;
	}

	/**
	 * @param nameForRule the nameForRule to set
	 */
	public void setNameForRule(String nameForRule) {
		this.nameForRule = nameForRule;
	}

	/**
	 * @return the emailForRule
	 */
	public String getEmailForRule() {
		return emailForRule;
	}

	/**
	 * @param emailForRule the emailForRule to set
	 */
	public void setEmailForRule(String emailForRule) {
		this.emailForRule = emailForRule;
	}
	
	public String toString() {
		
		StringBuilder sb = new StringBuilder();
		sb.append(isApply());
		sb.append("; ");
		sb.append(getResellerName());
		sb.append("; ");
		for (String currEmail : getEmailsFromInput()) {
			sb.append(currEmail);
			sb.append(", ");
		}
		sb.append("; ");
		sb.append(getNameForRule());
		sb.append("; ");
		sb.append(getEmailForRule());
		sb.append("; ");
		
		return sb.toString();
	}
	
}
