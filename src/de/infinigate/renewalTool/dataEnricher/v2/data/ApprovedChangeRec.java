package de.infinigate.renewalTool.dataEnricher.v2.data;

import java.util.HashMap;
import java.util.Map;

public class ApprovedChangeRec {

	/**
	 * The Id of the renewal record to which those changes apply.
	 */
	private int renewalRecId;
	
	/**
	 * Whether this change was approved positively.
	 */
	private boolean applyChange;
	
	/**
	 * List with suggested changes with key=fieldname, val=fieldval.
	 */
	private Map<String, String> changes = new HashMap<String, String>();
	
	public ApprovedChangeRec(int renewalRecId) {
		this.renewalRecId = renewalRecId;
		applyChange = true;	
	}
	
	public void addChange(String fieldName, String fieldVal) {
		changes.put(fieldName, fieldVal);
	}
	
	/**
	 * Getter for the map holding all changes to a rec.
	 * @return the changes
	 */
	public Map<String, String> getChanges() {
		return changes;
	}

	/**
	 * @return the renewalRecId
	 */
	public int getRenewalRecId() {
		return renewalRecId;
	}

	/**
	 * @param renewalRecId the renewalRecId to set
	 */
	public void setRenewalRecId(int renewalRecId) {
		this.renewalRecId = renewalRecId;
	}

	/**
	 * @return the applyChange
	 */
	public boolean isApplyChange() {
		return applyChange;
	}

	/**
	 * @param applyChange the applyChange to set
	 */
	public void setApplyChange(boolean applyChange) {
		this.applyChange = applyChange;
	}

}
