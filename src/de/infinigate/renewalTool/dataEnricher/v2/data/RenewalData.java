package de.infinigate.renewalTool.dataEnricher.v2.data;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * Class ment for the storage of a renewal-data record.
 * @author mathias.mueller
 */
public class RenewalData {

	/**
	 * Label of the field in the DB.
	 */
	public static final String PROFILE_CODE_COLNAME = "Import Profile Code";

	/**
	 * Label of the field in the DB.
	 */
	public static final String ID_COLNAME = "ID";
	
	/**
	 * Label of the field in the DB.
	 */
	public static final String RESELLER_NAME_COLNAME = "Reseller Name";
	
	/**
	 * Label of the field in the DB.
	 */
	public static final String CUSTOMER_NO_COLNAME = "NAV Customer No_";

	/**
	 * Label of the field in the DB.
	 */
	public static final String YOUR_REFERENCE_COLNAME = "Your Reference";

	/**
	 * Label of the field in the DB.
	 */
	public static final String END_USER_NAME_COLNAME = "EndUser Name";
	
	/**
	 * Label of the field in the DB.
	 */
	public static final String END_USER_COMPANY_COLNAME = "EndUser Company";
	
	/**
	 * Label of the field in the DB.
	 */
	public static final String END_USER_POST_CODE_COLNAME = "EndUser Post Code";

	/**
	 * Label of the field in the DB.
	 */
	public static final String END_USER_CITY_COLNAME = "EndUser City";

	/**
	 * Label of the field in the DB.
	 */
	public static final String END_USER_ADDRESS_COLNAME = "EndUser Address";

	/**
	 * Label of the field in the DB.
	 */
	public static final String LICENSED_USERS_COLNAME = "Licensed Users";

	/**
	 * Label of the field in the DB.
	 */
	public static final String EXPIRATION_DATE_COLNAME = "Expiration Date";

	/**
	 * Label of the field in the DB.
	 */
	public static final String ITEM_NO_COLNAME = "Item No_";

	/**
	 * Label of the field in the DB.
	 */
	public static final String ALT_ITEM_NO_COLNAME = "Alt_ Item No_";

	/**
	 * Label of the field in the DB.
	 */
	public static final String SALES_ORDER_NO_COLNAME = "Sales Order No_";

	/**
	 * Label of the field in the DB.
	 */
	public static final String DESCRIPTION_1_COLNAME = "Description 1";

	/**
	 * Label of the field in the DB.
	 */
	public static final String DESCRIPTION_2_COLNAME = "Description 2";

	/**
	 * Label of the field in the DB.
	 */
	public static final String DESCRIPTION_3_COLNAME = "Description 3";

	/**
	 * Label of the field in the DB.
	 */
	public static final String DESCRIPTION_4_COLNAME = "Description 4";

	/**
	 * Label of the field in the DB.
	 */
	public static final String SERIAL_NO_COLNAME = "Serial No_";

	/**
	 * Label of the field in the DB.
	 */
	public static final String DEVICE_TYPE_COLNAME = "Device Type";

	/**
	 * Label of the field in the DB.
	 */
	public static final String SUBSCRIPTION_TYPE_COLNAME = "Subscription Type";

	/**
	 * Label of the field in the DB.
	 */
	public static final String SALESPERSON_IDENT_CODE_COLNAME = "Salesperson Ident_ Code";

	/**
	 * Label of the field in the DB.
	 */
	public static final String E_MAIL_TO_COLNAME = "E-Mail TO";

	/**
	 * Label of the field in the DB.
	 */
	public static final String E_MAIL_CC_COLNAME = "E-Mail CC";

	/**
	 * Label of the field in the DB.
	 */
	public static final String E_MAIL_BCC_COLNAME = "E-Mail BCC";

	/**
	 * Label of the field in the DB.
	 */
	public static final String SALUTATION_COLNAME = "Salutation";

	/**
	 * Label of the field in the DB.
	 */
	public static final String RENEWAL_STATUS_COLNAME = "Renewal Status";

	/**
	 * Label of the field in the DB.
	 */
	public static final String PROFILE_SPECIAL_FIELD_1_COLNAME = "Profile Special Field 1";

	/**
	 * Label of the field in the DB.
	 */
	public static final String PROFILE_SPECIAL_FIELD_2_COLNAME = "Profile Special Field 2";

	/**
	 * Label of the field in the DB.
	 */
	public static final String PROFILE_SPECIAL_FIELD_3_COLNAME = "Profile Special Field 3";

	/**
	 * Label of the field in the DB.
	 */
	public static final String NAV_CONTACT_NO_COLNAME = "NAV Contact No_";
	
	/**
	 * Label of the field in the DB.
	 */
	public static final String OVERALL_NOTIFICATION_COLNAME = "Overall Notification";
	
	/**
	 * Label of the field in the DB.
	 */
	public static final String OVERWRITE_EMAIL_BODY_COLNAME = "Overwrite E-Mail Body"; 
	
	/**
	 * Field holding the value of the "Profile Code"-field.
	 */
	private String profileCode;

	/**
	 * Field holding the value of the "Id"-field.
	 */
	private int id;
	
	/**
	 * Field holding the value of the "Reseller Name"-field.
	 */
	private String resellerName;
	
	/**
	 * Field holding the value of the "Customer No."-field.
	 */
	private String customerNo;
	
	/**
	 * Field holding the value of the "Contact No."-field.
	 */
	private String contactNo;
	
	/**
	 * Field holding the value of the "Your Reference"-field.
	 */
	private String yourReference;
	
	/**
	 * Field holding the value of the "EndUser Name"-field.
	 */
	private String endUserName;
	
	/**
	 * Field holding the value of the "EndUser Company"-field.
	 */
	private String endUserCompany;
	
	/**
	 * Field holding the value of the "EndUser Post Code"-field.
	 */
	private String endUserPostCode;
	
	/**
	 * Field holding the value of the "EndUser City"-field.
	 */
	private String endUserCity;
	
	/**
	 * Field holding the value of the "EndUser Address"-field.
	 */
	private String endUserAddress;
	
	/**
	 * Field holding the value of the "Licensed Users"-field.
	 */
	private int licensedUsers;
	
	/**
	 * Field holding the value of the "Expiration Date"-field.
	 */
	private Date expirationDate;
	
	/**
	 * Field holding the value of the "Item No."-field.
	 */
	private String itemNo;
	
	/**
	 * Field holding the value of the "Alt. Item No."-field.
	 */
	private String altItemNo;
	
	/**
	 * Field holding the value of the "Sales Order No."-field.
	 */
	private String salesOrderNo;
	
	/**
	 * Field holding the value of the "Description 1"-field.
	 */
	private String description1;
	
	/**
	 * Field holding the value of the "Description 2"-field.
	 */
	private String description2;
	
	/**
	 * Field holding the value of the "Description 3"-field.
	 */
	private String description3;
	
	/**
	 * Field holding the value of the "Description 4"-field.
	 */
	private String description4;
	
	/**
	 * Field holding the value of the "Serial No."-field.
	 */
	private String serialNo;
	
	/**
	 * Field holding the value of the "Device Type"-field.
	 */
	private String deviceType;
	
	/**
	 * Field holding the value of the "Subscription Type"-field.
	 */
	private String subscriptionType;
	
	/**
	 * Field holding the value of the "Salesperson Ident. Code"-field.
	 */
	private String salespersonIdentCode;
	
	/**
	 * Field holding the value of the "E-Mail To"-field.
	 */
	private String eMailTo;
	
	/**
	 * Field holding the value of the "E-Mail Cc"-field.
	 */
	private String eMailCc;
	
	/**
	 * Field holding the value of the "E-Mail Bcc"-field.
	 */
	private String eMailBcc;
	
	/**
	 * Field holding the value of the "Salutation"-field.
	 */
	private String salutation;
	
	/**
	 * Field holding the value of the "Renewal Status"-field.
	 */
	private String renewalStatus;
	
	/**
	 * Field holding the value of the "Profile Special Field 1"-field.
	 */
	private String profileSpecialField1;
	
	/**
	 * Field holding the value of the "Profile Special Field 2"-field.
	 */
	private String profileSpecialField2;
	
	/**
	 * Field holding the value of the "Profile Special Field 3"-field.
	 */
	private String profileSpecialField3;
	
	/**
	 * Field holding the value of the "Overall Notification"-field.
	 */
	private boolean overallNotification = true;
	
	/**
	 * Field holding the value of the "Overwrite E-Mail Body"-field.
	 */
	private String overwriteEmailBody;
	
	/**
	 * The names of the columns that shall form the key for this set of renewalData
	 */
	private String[] keyCols;
	
	/**
	 * List of the Names of all fields, that are allowed in renewal body's middle parts.
	 */
	public static final String[] LEGAL_FIELDS_IN_RENEWAL_BODIES = new String[] {
			PROFILE_CODE_COLNAME,
			RESELLER_NAME_COLNAME, 	
			CUSTOMER_NO_COLNAME,
			YOUR_REFERENCE_COLNAME,
			END_USER_NAME_COLNAME,
			END_USER_COMPANY_COLNAME,
			END_USER_POST_CODE_COLNAME,
			END_USER_CITY_COLNAME,
			END_USER_ADDRESS_COLNAME,
			LICENSED_USERS_COLNAME,
			EXPIRATION_DATE_COLNAME,
			ITEM_NO_COLNAME,
			ALT_ITEM_NO_COLNAME,
			SALES_ORDER_NO_COLNAME,
			DESCRIPTION_1_COLNAME,
			DESCRIPTION_2_COLNAME,
			DESCRIPTION_3_COLNAME,
			DESCRIPTION_4_COLNAME,
			SERIAL_NO_COLNAME,
			DEVICE_TYPE_COLNAME,
			SUBSCRIPTION_TYPE_COLNAME,
			SALESPERSON_IDENT_CODE_COLNAME,
			RENEWAL_STATUS_COLNAME,
			NAV_CONTACT_NO_COLNAME,
			PROFILE_SPECIAL_FIELD_1_COLNAME,
			PROFILE_SPECIAL_FIELD_2_COLNAME,
			PROFILE_SPECIAL_FIELD_3_COLNAME
	};
	
	/**
	 * Configurable key-column.
	 */
	private String dynamicKey;

	/**
	 * Flag indicating whether all key-fields for this record could be obtained when trying to build the key. 
	 */
	private boolean keyComplete;
	
	/**
	 * Field indicating whether this record has been changed after retrieving it from the database.
	 */
	private boolean recordChanged = false;
	
	/**
	 * Constructor for an empty RenewalData record.
	 */
	public RenewalData() {
		
	}
	
	/**
	 * Constructor for a record of renewalData initializing fields.
	 * @param profileCode
	 * @param id
	 * @param resellerNo
	 * @param customerNo
	 * @param endUserCompany
	 * @param yourReference
	 * @param endUserName
	 * @param endUserPostCode
	 * @param endUserCity
	 * @param endUserAddress
	 * @param licensedUsers
	 * @param expirationDate
	 * @param itemNo
	 * @param altItemNo
	 * @param salesOrderNo
	 * @param description1
	 * @param description2
	 * @param description3
	 * @param description4
	 * @param serialNo
	 * @param deviceType
	 * @param subscriptionType
	 * @param salespersonIdentCode
	 * @param eMail
	 * @param salutation
	 * @param renewalStatus
	 * @param profileSpecialField1
	 * @param profileSpecialField2
	 * @param profileSpecialField3
	 * @param contactNo
	 * @param keyCols
	 */
	public RenewalData(String profileCode, int id, String resellerNo,
			String customerNo, String yourReference, String endUserName, String endUserCompany,
			String endUserPostCode, String endUserCity, String endUserAddress,
			int licensedUsers, Date expirationDate, String itemNo,
			String altItemNo, String salesOrderNo, String description1,
			String description2, String description3, String description4,
			String serialNo, String deviceType, String subscriptionType,
			String salespersonIdentCode, String eMail, String salutation,
			String renewalStatus, String profileSpecialField1,
			String profileSpecialField2, String profileSpecialField3, String contactNo, 
			String overwriteBodyCode, boolean overallNotification, String[] keyCols) {

		this.profileCode = profileCode;
		this.id = id;
		this.resellerName = resellerNo;
		this.customerNo = customerNo;
		this.yourReference = yourReference;
		this.endUserName = endUserName;
		this.endUserCompany = endUserCompany;
		this.endUserPostCode = endUserPostCode;
		this.endUserCity = endUserCity;
		this.endUserAddress = endUserAddress;
		this.licensedUsers = licensedUsers;
		this.expirationDate = expirationDate;
		this.itemNo = itemNo;
		this.altItemNo = altItemNo;
		this.salesOrderNo = salesOrderNo;
		this.description1 = description1;
		this.description2 = description2;
		this.description3 = description3;
		this.description4 = description4;
		this.serialNo = serialNo;
		this.deviceType = deviceType;
		this.subscriptionType = subscriptionType;
		this.salespersonIdentCode = salespersonIdentCode;
		this.eMailTo = eMail;
		this.salutation = salutation;
		this.renewalStatus = renewalStatus;
		this.profileSpecialField1 = profileSpecialField1;
		this.profileSpecialField2 = profileSpecialField2;
		this.profileSpecialField3 = profileSpecialField3;
		this.contactNo = contactNo;
		this.overwriteEmailBody = overwriteBodyCode;
		this.overallNotification = overallNotification;
		this.keyCols = keyCols;
		
		if (keyCols != null)
			buildDynamicKey();
	}

	/**
	 * @return the profileCode
	 */
	public String getProfileCode() {
		return profileCode;
	}

	/**
	 * @param profileCode the profileCode to set
	 */
	public void setProfileCode(String profileCode) {
		if (!this.profileCode.equals(profileCode))
			recordChanged = true;
		this.profileCode = profileCode;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		if (this.id != id) 
			recordChanged = true;
		this.id = id;
	}

	/**
	 * @return the resellerNo
	 */
	public String getResellerName() {
		return resellerName;
	}

	/**
	 * @param resellerName the resellerName to set
	 */
	public void setResellerName(String resellerName) {
		if (!this.resellerName.equals(resellerName))
			recordChanged = true;
		this.resellerName = resellerName;
	}

	/**
	 * @return the customerNo
	 */
	public String getCustomerNo() {
		return customerNo;
	}

	/**
	 * @param customerNo the customerNo to set
	 */
	public void setCustomerNo(String customerNo) {
		if (!this.customerNo.equals(customerNo))
			recordChanged = true;
		this.customerNo = customerNo;
	}

	/**
	 * @return the yourReference
	 */
	public String getYourReference() {
		return yourReference;
	}

	/**
	 * @param yourReference the yourReference to set
	 */
	public void setYourReference(String yourReference) {
		if (!this.yourReference.equals(yourReference))
			recordChanged = true;
		this.yourReference = yourReference;
	}

	/**
	 * @return the endUserName
	 */
	public String getEndUserName() {
		return endUserName;
	}

	/**
	 * @param endUserName the endUserName to set
	 */
	public void setEndUserName(String endUserName) {
		if (!this.endUserName.equals(endUserName)) 
			recordChanged = true;
		this.endUserName = endUserName;
	}

	/**
	 * @return the endUserPostCode
	 */
	public String getEndUserPostCode() {
		return endUserPostCode;
	}

	/**
	 * @param endUserPostCode the endUserPostCode to set
	 */
	public void setEndUserPostCode(String endUserPostCode) {
		if (!this.endUserPostCode.equals(endUserPostCode)) 
			recordChanged = true;
		this.endUserPostCode = endUserPostCode;
	}

	/**
	 * @return the endUserCity
	 */
	public String getEndUserCity() {
		return endUserCity;
	}

	/**
	 * @param endUserCity the endUserCity to set
	 */
	public void setEndUserCity(String endUserCity) {
		if (!this.endUserCity.equals(endUserCity))
			recordChanged = true;
		this.endUserCity = endUserCity;
	}

	/**
	 * @return the endUserAddress
	 */
	public String getEndUserAddress() {
		return endUserAddress;
	}

	/**
	 * @param endUserAddress the endUserAddress to set
	 */
	public void setEndUserAddress(String endUserAddress) {
		if (!this.endUserAddress.equals(endUserAddress))
			recordChanged = true;
		this.endUserAddress = endUserAddress;
	}

	/**
	 * @return the licensedUsers
	 */
	public int getLicensedUsers() {
		return licensedUsers;
	}

	/**
	 * @param licensedUsers the licensedUsers to set
	 */
	public void setLicensedUsers(int licensedUsers) {
		if (this.licensedUsers != licensedUsers) 
			recordChanged = true;
		this.licensedUsers = licensedUsers;
	}

	/**
	 * @return the expirationDate
	 */
	public Date getExpirationDate() {
		return expirationDate;
	}

	/**
	 * @param expirationDate the expirationDate to set
	 */
	public void setExpirationDate(Date expirationDate) {
		if (!this.expirationDate.equals(expirationDate))
			recordChanged = true;
		this.expirationDate = expirationDate;
	}

	/**
	 * @return the itemNo
	 */
	public String getItemNo() {
		return itemNo;
	}

	/**
	 * @param itemNo the itemNo to set
	 */
	public void setItemNo(String itemNo) {
		if (!this.itemNo.equals(itemNo))
			recordChanged = true;
		this.itemNo = itemNo;
	}

	/**
	 * @return the altItemNo
	 */
	public String getAltItemNo() {
		return altItemNo;
	}

	/**
	 * @param altItemNo the altItemNo to set
	 */
	public void setAltItemNo(String altItemNo) {
		if (!this.altItemNo.equals(altItemNo))
			recordChanged = true;
		this.altItemNo = altItemNo;
	}

	/**
	 * @return the salesOrderNo
	 */
	public String getSalesOrderNo() {
		return salesOrderNo;
	}

	/**
	 * @param salesOrderNo the salesOrderNo to set
	 */
	public void setSalesOrderNo(String salesOrderNo) {
		if(!this.salesOrderNo.equals(salesOrderNo))		
			recordChanged = true;
		this.salesOrderNo = salesOrderNo;
	}

	/**
	 * @return the description1
	 */
	public String getDescription1() {
		return description1;
	}

	/**
	 * @param description1 the description1 to set
	 */
	public void setDescription1(String description1) {
		if (!this.description1.equals(description1)) 
			recordChanged = true;
		this.description1 = description1;
	}

	/**
	 * @return the description2
	 */
	public String getDescription2() {
		return description2;
	}

	/**
	 * @param description2 the description2 to set
	 */
	public void setDescription2(String description2) {
		if (!this.description2.equals(description2)) 
			recordChanged = true;
		this.description2 = description2;
	}

	/**
	 * @return the description3
	 */
	public String getDescription3() {
		return description3;
	}

	/**
	 * @param description3 the description3 to set
	 */
	public void setDescription3(String description3) {
		if (!this.description3.equals(description3)) 
			recordChanged = true;
		this.description3 = description3;
	}

	/**
	 * @return the description4
	 */
	public String getDescription4() {
		return description4;
	}

	/**
	 * @param description4 the description4 to set
	 */
	public void setDescription4(String description4) {
		if (!this.description4.equals(description4)) 
			recordChanged = true;
		this.description4 = description4;
	}

	/**
	 * @return the serialNo
	 */
	public String getSerialNo() {
		return serialNo;
	}

	/**
	 * @param serialNo the serialNo to set
	 */
	public void setSerialNo(String serialNo) {
		if (!this.serialNo.equals(serialNo))
			recordChanged = true;
		this.serialNo = serialNo;
	}

	/**
	 * @return the deviceType
	 */
	public String getDeviceType() {
		return deviceType;
	}

	/**
	 * @param deviceType the deviceType to set
	 */
	public void setDeviceType(String deviceType) {
		if (!this.deviceType.equals(deviceType))
			recordChanged = true;
		this.deviceType = deviceType;
	}

	/**
	 * @return the subscriptionType
	 */
	public String getSubscriptionType() {
		return subscriptionType;
	}

	/**
	 * @param subscriptionType the subscriptionType to set
	 */
	public void setSubscriptionType(String subscriptionType) {
		if (!this.subscriptionType.equals(subscriptionType))
			recordChanged = true;
		this.subscriptionType = subscriptionType;
	}

	/**
	 * @return the salespersonIdentCode
	 */
	public String getSalespersonIdentCode() {
		return salespersonIdentCode;
	}

	/**
	 * @param salespersonIdentCode the salespersonIdentCode to set
	 */
	public void setSalespersonIdentCode(String salespersonIdentCode) {
		if (!this.salespersonIdentCode.equals(salespersonIdentCode))
			recordChanged = true;
		this.salespersonIdentCode = salespersonIdentCode;
	}

	/**
	 * @return the eMail
	 */
	public String geteMailTo() {
		return eMailTo;
	}

	/**
	 * @param eMailTo the eMail to set
	 */
	public void seteMailTo(String eMailTo) {
		if (!this.eMailTo.equals(eMailTo))
			recordChanged = true;
		this.eMailTo = eMailTo;
	}

	/**
	 * @return the salutation
	 */
	public String getSalutation() {
		return salutation;
	}

	/**
	 * @param salutation the salutation to set
	 */
	public void setSalutation(String salutation) {
		if (!this.salutation.equals(salutation))
			recordChanged = true;
		this.salutation = salutation;
	}

	/**
	 * @return the renewalStatus
	 */
	public String getRenewalStatus() {
		return renewalStatus;
	}

	/**
	 * @param renewalStatus the renewalStatus to set
	 */
	public void setRenewalStatus(String renewalStatus) {
		if (!this.renewalStatus.equals(renewalStatus))
			recordChanged = true;
		this.renewalStatus = renewalStatus;
	}

	/**
	 * @return the profileSpecialField1
	 */
	public String getProfileSpecialField1() {
		return profileSpecialField1;
	}

	/**
	 * @param profileSpecialField1 the profileSpecialField1 to set
	 */
	public void setProfileSpecialField1(String profileSpecialField1) {
		if (!this.profileSpecialField1.equals(profileSpecialField1))
			recordChanged = true;
		this.profileSpecialField1 = profileSpecialField1;
	}

	/**
	 * @return the profileSpecialField2
	 */
	public String getProfileSpecialField2() {
		return profileSpecialField2;
	}

	/**
	 * @param profileSpecialField2 the profileSpecialField2 to set
	 */
	public void setProfileSpecialField2(String profileSpecialField2) {
		if (!this.profileSpecialField2.equals(profileSpecialField3))
			recordChanged = true;
		this.profileSpecialField2 = profileSpecialField2;
	}

	/**
	 * @return the profileSpecialField3
	 */
	public String getProfileSpecialField3() {
		return profileSpecialField3;
	}

	/**
	 * @param profileSpecialField3 the profileSpecialField3 to set
	 */
	public void setProfileSpecialField3(String profileSpecialField3) {
		if (!this.profileSpecialField3.equals(profileSpecialField3))
			recordChanged = true;
		this.profileSpecialField3 = profileSpecialField3;
	}

	/**
	 * Returns the configurable key for this renewalData record.
	 * @return the key
	 */
	public String getDynamicKey() {
		if (dynamicKey == null)
			buildDynamicKey();
		return dynamicKey;
	}
	
	/**
	 * Simple toString method.
	 */
	public String toString() {
		return "Id: " + id + ", Reseller No.:" + resellerName;
	}
	
	/**
	 * Getter for the 'recordChanged'-flag of this Class.
	 * @return whether this record was changed after retrieving it from the database
	 */
	public boolean isChanged() {
		return recordChanged;
	}

	/**
	 * Builds a key, whose composition can be set dynamically by the user.
	 * @return the key as semicolon-separated String
	 */
	public String buildDynamicKey() {
		
		StringBuilder sb = new StringBuilder();
		
		boolean colMissing = false;
		
		int i = 0;
		for (String currKeyCol : keyCols) {
			
			if (i++ > 0)
				sb.append("; ");
			
			if (currKeyCol.equals(PROFILE_CODE_COLNAME)) {
				if (getProfileCode() == null || getProfileCode().equals(" "))
					colMissing = true;
				else
					sb.append(getProfileCode());
			} else if (currKeyCol.equals(CUSTOMER_NO_COLNAME)) {
				if (getCustomerNo() == null || getCustomerNo().equals(" "))
					colMissing = true;
				else
					sb.append(getCustomerNo());
			} else if (currKeyCol.equals(YOUR_REFERENCE_COLNAME)) {
				if (getYourReference() == null || getYourReference().equals(" "))
					colMissing = true;
				else
					sb.append(getYourReference());
			} else if (currKeyCol.equals(END_USER_NAME_COLNAME)) {
				if (getEndUserName() == null || getEndUserName().equals(" "))
					colMissing = true;
				else
					sb.append(getEndUserName());
			} else if (currKeyCol.equals(END_USER_POST_CODE_COLNAME)) {
				if (getEndUserPostCode() == null || getEndUserPostCode().equals(" "))
					colMissing = true;
				else
					sb.append(getEndUserPostCode());
			} else if (currKeyCol.equals(END_USER_CITY_COLNAME)) {
				if (getEndUserCity() == null || getEndUserCity().equals(" "))
					colMissing = true;
				else
					sb.append(getEndUserCity());
			} else if (currKeyCol.equals(END_USER_ADDRESS_COLNAME)) {
				if (getEndUserAddress() == null || getEndUserAddress().equals(" "))
					colMissing = true;
				else
					sb.append(getEndUserAddress());
			} else if (currKeyCol.equals(LICENSED_USERS_COLNAME)) {
				sb.append(getLicensedUsers());
			} else if (currKeyCol.equals(EXPIRATION_DATE_COLNAME)) {
				if (getExpirationDate() == null || getExpirationDate().equals(" "))
					colMissing = true;
				else
					sb.append(getExpirationDate());
			} else if (currKeyCol.equals(ITEM_NO_COLNAME)) {
				if (getItemNo() == null || getItemNo().equals(" "))
					colMissing = true;
				else
					sb.append(getItemNo());
			} else if (currKeyCol.equals(ALT_ITEM_NO_COLNAME)) {
				if (getAltItemNo() == null || getAltItemNo().equals(" "))
					colMissing = true;
				else
					sb.append(getAltItemNo());
			} else if (currKeyCol.equals(SALES_ORDER_NO_COLNAME)) {
				if (getSalesOrderNo() == null || getSalesOrderNo().equals(" "))
					colMissing = true;
				else
					sb.append(getSalesOrderNo());
			} else if (currKeyCol.equals(DESCRIPTION_1_COLNAME)) {
				if (getDescription1() == null || getDescription1().equals(" "))
					colMissing = true;
				else
					sb.append(getDescription1());
			} else if (currKeyCol.equals(DESCRIPTION_2_COLNAME)) {
				if (getDescription2() == null || getDescription2().equals(" "))
					colMissing = true;
				else
					sb.append(getDescription2());
			} else if (currKeyCol.equals(DESCRIPTION_3_COLNAME)) {
				if (getDescription3() == null || getDescription3().equals(" "))
					colMissing = true;
				else
					sb.append(getDescription3());
			} else if (currKeyCol.equals(DESCRIPTION_4_COLNAME)) {
				if (getDescription4() == null || getDescription4().equals(" "))
					colMissing = true;
				else
					sb.append(getDescription4());
			} else if (currKeyCol.equals(SERIAL_NO_COLNAME)) {
				if (getSerialNo() == null || getSerialNo().equals(" "))
					colMissing = true;
				else
					sb.append(getSerialNo());
			} else if (currKeyCol.equals(DEVICE_TYPE_COLNAME)) {
				if (getDeviceType() == null || getDeviceType().equals(" "))
					colMissing = true;
				else
					sb.append(getDeviceType());
			} else if (currKeyCol.equals(SUBSCRIPTION_TYPE_COLNAME)) {
				if (getSubscriptionType() == null || getSubscriptionType().equals(" "))
					colMissing = true;
				else
					sb.append(getSubscriptionType());
			} else if (currKeyCol.equals(SALESPERSON_IDENT_CODE_COLNAME)) {
				if (getSalespersonIdentCode() == null || getSalespersonIdentCode().equals(" "))
					colMissing = true;
				else
					sb.append(getSalespersonIdentCode());
			} else if (currKeyCol.equals(E_MAIL_TO_COLNAME)) {
				if (geteMailTo() == null || geteMailTo().equals(" "))
					colMissing = true;
				else
					sb.append(geteMailTo());
			} else if (currKeyCol.equals(E_MAIL_CC_COLNAME)) {
				if (geteMailCc() == null || geteMailCc().equals(" "))
					colMissing = true;
				else
					sb.append(geteMailCc());
			} else if (currKeyCol.equals(E_MAIL_BCC_COLNAME)) {
				if (geteMailBcc() == null || geteMailBcc().equals(" "))
					colMissing = true;
				else
					sb.append(geteMailBcc());
			} else if (currKeyCol.equals(SALUTATION_COLNAME)) {
				if (getSalutation() == null || getSalutation().equals(" "))
					colMissing = true;
				else
					sb.append(getSalutation());
			} else if (currKeyCol.equals(RENEWAL_STATUS_COLNAME)) {
				if (getRenewalStatus() == null || getRenewalStatus().equals(" "))
					colMissing = true;
				else
					sb.append(getRenewalStatus());
			} else if (currKeyCol.equals(PROFILE_SPECIAL_FIELD_1_COLNAME)) {
				if (getProfileSpecialField1() == null || getProfileSpecialField1().equals(" "))
					colMissing = true;
				else
					sb.append(getProfileSpecialField1());
			} else if (currKeyCol.equals(PROFILE_SPECIAL_FIELD_2_COLNAME)) {
				if (getProfileSpecialField2() == null || getProfileSpecialField2().equals(" "))
					colMissing = true;
				else
					sb.append(getProfileSpecialField2());
			} else if (currKeyCol.equals(PROFILE_SPECIAL_FIELD_3_COLNAME)) {
				if (getProfileSpecialField3() == null || getProfileSpecialField3().equals(" "))
					colMissing = true;
				else
					sb.append(getProfileSpecialField3());
			} else if (currKeyCol.equals(OVERALL_NOTIFICATION_COLNAME)) {
				sb.append(isOverallNotification());
			} else if (currKeyCol.equals(OVERWRITE_EMAIL_BODY_COLNAME)) {
				if (getOverwriteEmailBody() == null || getOverwriteEmailBody().equals(" "))
					colMissing = true;
				else
					sb.append(getOverwriteEmailBody());
			} 
		}
		
		keyComplete = !colMissing;
		
		return sb.toString();
		
	}

	/**
	 * Default getter for this flag.
	 * @return if the key is complete
	 */
	boolean isKeyComplete() {
		return keyComplete;
	}
	
	/**
	 * Method that enables one to add a column value by it's name.
	 * @param name the name of the column for which a value shall be set
	 * @param val the value that shall be set for the column with the specified name
	 * @throws ParseException in case a val is passed for the "Expiration Date"-column that is invalid
	 * @throws NumberFormatException in case a "Licensed Users"-val couldn't be parsed successfully
	 */
	public void updateFieldByName(String name, String val) throws ParseException, NumberFormatException {
		
		if (val != null && !val.equals("")) {
	 		if (name.equals(PROFILE_CODE_COLNAME)) {
				setProfileCode(val);
			} else if (name.equals(ID_COLNAME)) {
				setId(Integer.parseInt(val));
			} else if (name.equals(RESELLER_NAME_COLNAME)) {
				setResellerName(val);
			} else if (name.equals(CUSTOMER_NO_COLNAME)) {
				setCustomerNo(val.toUpperCase());
			} else if (name.equals(YOUR_REFERENCE_COLNAME)) {
				setYourReference(val);
			} else if (name.equals(END_USER_NAME_COLNAME)) {
				setEndUserName(val);
			} else if (name.equals(END_USER_COMPANY_COLNAME)) {
				setEndUserCompany(val);
			} else if (name.equals(END_USER_POST_CODE_COLNAME)) {
				setEndUserPostCode(val);
			} else if (name.equals(END_USER_CITY_COLNAME)) {
				setEndUserCity(val);
			} else if (name.equals(END_USER_ADDRESS_COLNAME)) {
				setEndUserAddress(val);
			} else if (name.equals(LICENSED_USERS_COLNAME)) {
				int decimalSignPos = -1;
				if (val.contains("."))
					decimalSignPos = val.indexOf(".");
				if (val.contains(","))
					decimalSignPos = val.indexOf(",");
				if (decimalSignPos >= 0)
					val = val.substring(0,decimalSignPos);
				
				setLicensedUsers(Integer.parseInt(val));
				
			} else if (name.equals(EXPIRATION_DATE_COLNAME)) {
				SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
				setExpirationDate(new Date(sdf.parse(val).getTime()));
			} else if (name.equals(ITEM_NO_COLNAME)) {
				setItemNo(val);
			} else if (name.equals(ALT_ITEM_NO_COLNAME)) {
				setAltItemNo(val);
			} else if (name.equals(SALES_ORDER_NO_COLNAME)) {
				setSalesOrderNo(val);
			} else if (name.equals(DESCRIPTION_1_COLNAME)) {
				setDescription1(val);
			} else if (name.equals(DESCRIPTION_2_COLNAME)) {
				setDescription2(val);
			} else if (name.equals(DESCRIPTION_3_COLNAME)) {
				setDescription3(val);
			} else if (name.equals(DESCRIPTION_4_COLNAME)) {
				setDescription4(val);
			} else if (name.equals(SERIAL_NO_COLNAME)) {
				setSerialNo(val);
			} else if (name.equals(DEVICE_TYPE_COLNAME)) {
				setDeviceType(val);
			} else if (name.equals(SUBSCRIPTION_TYPE_COLNAME)) {
				setSubscriptionType(val);
			} else if (name.equals(SALESPERSON_IDENT_CODE_COLNAME)) {
				setSalespersonIdentCode(val);
			} else if (name.equals(E_MAIL_TO_COLNAME)) {
				seteMailTo(val);
			} else if (name.equals(E_MAIL_CC_COLNAME)) {
				seteMailCc(val);
			} else if (name.equals(E_MAIL_BCC_COLNAME)) {
				seteMailBcc(val);
			} else if (name.equals(SALUTATION_COLNAME)) {
				setSalutation(val);
			} else if (name.equals(RENEWAL_STATUS_COLNAME)) {
				setRenewalStatus(val);
			} else if (name.equals(PROFILE_SPECIAL_FIELD_1_COLNAME)) {
				setProfileSpecialField1(val);
			} else if (name.equals(PROFILE_SPECIAL_FIELD_2_COLNAME)) {
				setProfileSpecialField2(val);
			} else if (name.equals(PROFILE_SPECIAL_FIELD_3_COLNAME)) {
				setProfileSpecialField3(val);
			} else if (name.equals(NAV_CONTACT_NO_COLNAME)) {
				setContactNo(val.toUpperCase());
			} else if (name.equals(OVERALL_NOTIFICATION_COLNAME)) {
				setOverallNotification(Boolean.getBoolean(val));
			} else if (name.equals(OVERWRITE_EMAIL_BODY_COLNAME)) {
				setOverwriteEmailBody(val);
			}
		}
	}
	
	public String getFieldValByName(String name) {
		if (name != null && !name.equals("")) {
			
			if (name.equals(PROFILE_CODE_COLNAME)) { 
				return getProfileCode();
			} else if (name.equals(ID_COLNAME)) {
				return Integer.toString(getId());
			} else if (name.equals(RESELLER_NAME_COLNAME)) {
				return getResellerName();
			} else if (name.equals(CUSTOMER_NO_COLNAME)) {
				return getCustomerNo();
			} else if (name.equals(YOUR_REFERENCE_COLNAME)) {
				return getYourReference();
			} else if (name.equals(END_USER_NAME_COLNAME)) {
				return getEndUserName();
			} else if (name.equals(END_USER_COMPANY_COLNAME)) {
				return getEndUserCompany();
			} else if (name.equals(END_USER_POST_CODE_COLNAME)) {
				return getEndUserPostCode();
			} else if (name.equals(END_USER_CITY_COLNAME)) {
				return getEndUserCity();
			} else if (name.equals(END_USER_ADDRESS_COLNAME)) {
				return getEndUserAddress();
			} else if (name.equals(LICENSED_USERS_COLNAME)) {
				int licensedUsersVal = getLicensedUsers();
				if (licensedUsersVal != 0)
					return Integer.toString(getLicensedUsers());
				else
					return "-";
			} else if (name.equals(EXPIRATION_DATE_COLNAME)) {
				return getExpirationDate().toString();
			} else if (name.equals(ITEM_NO_COLNAME)) {
				return getItemNo();
			} else if (name.equals(ALT_ITEM_NO_COLNAME)) {
				return getAltItemNo();
			} else if (name.equals(SALES_ORDER_NO_COLNAME)) {
				return getSalesOrderNo();
			} else if (name.equals(DESCRIPTION_1_COLNAME)) {
				return getDescription1();
			} else if (name.equals(DESCRIPTION_2_COLNAME)) {
				return getDescription2();
			} else if (name.equals(DESCRIPTION_3_COLNAME)) {
				return getDescription3();
			} else if (name.equals(DESCRIPTION_4_COLNAME)) {
				return getDescription4();
			} else if (name.equals(SERIAL_NO_COLNAME)) {
				return getSerialNo();
			} else if (name.equals(DEVICE_TYPE_COLNAME)) {
				return getDeviceType();
			} else if (name.equals(SUBSCRIPTION_TYPE_COLNAME)) {
				return getSubscriptionType();
			} else if (name.equals(SALESPERSON_IDENT_CODE_COLNAME)) {
				return getSalespersonIdentCode();
			} else if (name.equals(E_MAIL_TO_COLNAME)) {
				return geteMailTo();
			} else if (name.equals(E_MAIL_CC_COLNAME)) {
				return geteMailBcc();
			} else if (name.equals(E_MAIL_BCC_COLNAME)) {
				return geteMailBcc();
			} else if (name.equals(SALUTATION_COLNAME)) {
				return getSalutation();
			} else if (name.equals(RENEWAL_STATUS_COLNAME)) {
				return getRenewalStatus();
			} else if (name.equals(PROFILE_SPECIAL_FIELD_1_COLNAME)) {
				return getProfileSpecialField1();
			} else if (name.equals(PROFILE_SPECIAL_FIELD_2_COLNAME)) {
				return getProfileSpecialField2();
			} else if (name.equals(PROFILE_SPECIAL_FIELD_3_COLNAME)) {
				return getProfileSpecialField3();
			} else if (name.equals(NAV_CONTACT_NO_COLNAME)) {
				return getContactNo();
			} else if (name.equals(OVERALL_NOTIFICATION_COLNAME)) {
				return isOverallNotification() ? "Overall" : "Particular";
			} else if (name.equals(OVERWRITE_EMAIL_BODY_COLNAME)) {
				return getOverwriteEmailBody();
			}

		}
		return null;
	}

	public String getEndUserCompany() {
		return endUserCompany;
	}

	public void setEndUserCompany(String endUserCompany) {
		if (!this.endUserCompany.equals(endUserCompany))
			recordChanged = true;
		this.endUserCompany = endUserCompany;
	}

	public String getContactNo() {
		return contactNo;
	}

	public void setContactNo(String contactNo) {
		if (!this.contactNo.equals(contactNo))
			recordChanged = true;
		this.contactNo = contactNo;
	}

	public String geteMailCc() {
		return eMailCc;
	}

	public void seteMailCc(String eMailCc) {
		if (!this.eMailCc.equals(eMailCc))
			recordChanged = true;
		
		this.eMailCc = eMailCc;
	}

	public String geteMailBcc() {
		return eMailBcc;
	}

	public void seteMailBcc(String eMailBcc) {
		if (!this.eMailBcc.equals(eMailBcc));
		this.eMailBcc = eMailBcc;
	}

	public boolean isOverallNotification() {
		return overallNotification;
	}

	public void setOverallNotification(boolean overallNotification) {
		if (this.overallNotification != overallNotification)
			recordChanged = true;
		this.overallNotification = overallNotification;
	}

	public String getOverwriteEmailBody() {
		return overwriteEmailBody;
	}

	public void setOverwriteEmailBody(String overwriteEmailBody) {
		if (!this.overwriteEmailBody.equals(overwriteEmailBody))
			 recordChanged = true;
		this.overwriteEmailBody = overwriteEmailBody.toUpperCase();
	}
	
}