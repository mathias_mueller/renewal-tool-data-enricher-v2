package de.infinigate.renewalTool.dataEnricher.v2.data;

/**
 * Internal representation of a reseller specific configuration record.
 * @author mathias.mueller
 */
public class ResellerRuleData {

	/**
	 * The name of the reseller to which this config record applies.
	 */
	final String resellerName;
	
	/**
	 * The E-Mail TO to use for the reseller.
	 */
	final String overwriteTo;
	
	/**
	 * The E-Mail CC to use for the reseller.
	 */
	final String overwriteCc;
	
	/**
	 * The E-Mail BCC to use for the reseller.
	 */
	final String overwriteBcc;
	
	/**
	 * The Salutation that should overwrite the standard salutation for the reseller.
	 */
	final String overwriteSalutation;
	
	/**
	 * The code of the body that should overwrite the one normally used when sending out mails to the reseller.
	 */
	final String overwriteBodyCode;
	
	/**
	 * Whether the renewal mails should be sent as a complete mail or split by endusers.
	 */
	final boolean notificationTypeOverall;
		
	/**
	 * Standard CTor for a ResellerSpecConfigData record filling fields.
	 * @param resellerName the resellerName
	 * @param overwriteTo the overwriteTo
	 * @param overwriteCc the overwriteCc
	 * @param overwriteBcc the overwriteBcc
	 * @param overwriteSalutation the overwriteSalutation
	 * @param overwriteBodyCode the overwriteBodyCode
	 * @param notificationTypeOverall the notificationType
	 */
	public ResellerRuleData(String resellerName, String overwriteTo,
			String overwriteCc, String overwriteBcc,
			String overwriteSalutation, String overwriteBodyCode,
			boolean notificationTypeOverall) {
	
		this.resellerName = resellerName;
		this.overwriteTo = overwriteTo;
		this.overwriteCc = overwriteCc;
		this.overwriteBcc = overwriteBcc;
		this.overwriteSalutation = overwriteSalutation;
		this.overwriteBodyCode = overwriteBodyCode;
		this.notificationTypeOverall = notificationTypeOverall;
	}
	
	/**
	 * Getter.
	 * @return the ressellerName
	 */
	public String getResellerName() {
		return resellerName;
	}

	/**
	 * Getter.
	 * @return the overwriteTo
	 */
	public String getOverwriteTo() {
		return overwriteTo;
	}

	/**
	 * Getter.
	 * @return the overwriteCc
	 */
	public String getOverwriteCc() {
		return overwriteCc;
	}

	/**
	 * Getter.
	 * @return the overwriteBcc
	 */
	public String getOverwriteBcc() {
		return overwriteBcc;
	}

	/**
	 * Getter.
	 * @return the overwriteSalutation
	 */
	public String getOverwriteSalutation() {
		return overwriteSalutation;
	}

	/**
	 * Getter.
	 * @return the overwriteBody
	 */
	public String getOverwriteBody() {
		return overwriteBodyCode;
	}

	/**
	 * Getter
	 * @return the flag notificationTypeOverall
	 */
	public boolean isNotificationTypeOverall() {
		return notificationTypeOverall;
	}
}
