package de.infinigate.renewalTool.dataEnricher.v2.gui;

import javax.swing.JTable;

import de.infinigate.renewalTool.dataEnricher.v2.data.AmbiguousEmailData;

/**
 * Custom JTable that offers some additional features needed here.
 * @author mathias.mueller
 *
 */
public class AmbiguousEmailTable extends JTable {

	/**
	 * Generated serialVersionUID.
	 */
	private static final long serialVersionUID = 7171144483613580790L;
	
	public AmbiguousEmailTable(AmbiguousEmailData[] testData) {
		super(new AmbiguousEmailTableModel(testData));
		
		AmbiguousEmailTableModel tm = (AmbiguousEmailTableModel) getModel();
		
		for (int i = 0; i < tm.getRowCount(); i++) {
			setRowHeight(i, tm.getNoOfNecessaryLinesForRow(i) * 15);
		}
		getColumn(getColumnName(2)).setCellRenderer(new MultilineTextRenderer());
	}
	
	public void setData(AmbiguousEmailData[] dataToSet) {
		((AmbiguousEmailTableModel) this.getModel()).setData(dataToSet);
	}
	
	public AmbiguousEmailData[] getData() {
		return ((AmbiguousEmailTableModel) this.getModel()).getData();
	}

}
