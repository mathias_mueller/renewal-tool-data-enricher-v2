package de.infinigate.renewalTool.dataEnricher.v2.gui;

import javax.swing.table.AbstractTableModel;

import de.infinigate.renewalTool.dataEnricher.v2.data.AmbiguousEmailData;

/**
 * Custom table model that holds the data for the AmbiguousEmailTable.
 * @author mathias.mueller
 */
public class AmbiguousEmailTableModel extends AbstractTableModel {

	/**
	 * Generated serialVersionUID.
	 */
	private static final long serialVersionUID = -4729820072821767697L;

	/**
	 * The column names that should be used.
	 */
	public String[] columnNames = {"<html>Create/apply <br />rule?</html>", "<html>Reseller<br />Name</html>", "<html>E-Mails in<br />Inputfile</html>", "<html>Name for<br />new rule</html>", "<html>E-Mail<br />override</html>"};
	
	/**
	 * The data.
	 */
	public AmbiguousEmailData[] data;
	
	public AmbiguousEmailTableModel(AmbiguousEmailData[] dataToSet) {
		data = dataToSet;
	}
	
	/**
	 * Retrieves the total number of columns.
	 */
	public int getColumnCount() {

		return columnNames.length; 
	}

	/**
	 * Retrieves the total number of data-rows.
	 */
	public int getRowCount() {
		
		return data.length;
		
	}
	
	/**
	 * Returns the java-class for the data in the column specified by the parameter.
	 * @param col the column in question
	 */
	public Class<?> getColumnClass(int col) {
		try {
			switch (col) {
			
			case 0:
				return Class.forName("java.lang.Boolean");
				
			default:
				return Class.forName("java.lang.String");

			}
		} catch (ClassNotFoundException e) {
			System.err.println("Critical internal error. Something is very wrong with Java.");
		}
		
		return "".getClass();
	}
	
	/**
	 * Returns the name of the column with the specified index.
	 */
	public String getColumnName(int col) {
		return columnNames[col];
	}
	
	/**
	 * Returns if the cell specified by the parameters is editable.
	 * @param row the row of the cell in question
	 * @param col the column of the cell in question
	 */
	public boolean isCellEditable(int row, int col) {
		if (col == 1 || col == 2)
			return false;
		else
			return true;
	}

	/**
	 * Returns the value of the cell specified by the parameters.
	 * @param row the row of the cell in question
	 * @param col the column of the cell in question
	 * @return the value
	 */
	public Object getValueAt(int row, int col) {
		
		switch (col) {
		case 0:
			return data[row].isApply();
		case 1:
			return data[row].getResellerName();
		case 2:
			String [] allMails = data[row].getEmailsFromInput();
			if (allMails.length < 1)
				return "";
			StringBuilder sb = new StringBuilder(0);
			for (String currString : allMails) {
				if (sb.length() > 0)
					sb.append(";\n");
				if (currString == null) 
					sb.append("-"); 
				else
					sb.append(currString);
			}
			return sb.toString();
		case 3:
			return data[row].getNameForRule();
		case 4:
			return data[row].getEmailForRule();
		default:
			return "Value couldn't be retrieved";
		}

	}
	
	/**
	 * Sets the value of the specified cell to the value given.
	 * @param aValue the value to set	 
	 * @param row the row of the cell in question
	 * @param col the column of the cell in question
	 */
	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
		
		switch (columnIndex) {
		
		case 0:
			Boolean objValue = (Boolean) aValue;
			data[rowIndex].setApply(objValue);
			break;
			
		case 3:
			data[rowIndex].setNameForRule((String) aValue);
			break;
			
		case 4:
			data[rowIndex].setEmailForRule((String) aValue);
			break;
			
		default:
			System.err.println("Field that is not editable should have been updated.");

		}
		
	}
	
	/**
	 * Returns the number of lines that the table row should have (= the number of different email-addresses in the field "E-Mails in Input-file" of data).
	 * @param row the row in question 
	 * @return the number of different email-addresses in the "E-Mails in Input-file" column and therefore the number of lines this row should have
	 */
	public int getNoOfNecessaryLinesForRow(int row) {
		return data[row].getEmailsFromInput().length;
	}
	
	/**
	 * Getter.
	 * @return the Data
	 */
	public AmbiguousEmailData[] getData() {
		return data;
	}
	
	/**
	 * Setter.
	 * @param dataToSet the data that should be set
	 */
	public void setData(AmbiguousEmailData[] dataToSet) {
		data = dataToSet;
	}

}
