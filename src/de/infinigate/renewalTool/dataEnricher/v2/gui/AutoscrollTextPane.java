package de.infinigate.renewalTool.dataEnricher.v2.gui;

import java.awt.Color;

import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultStyledDocument;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;

/**
 * Simple extension to a JTextArea, that is able to automatically scroll a joined JScrollBar to its end when text is inserted.
 * @author mathias.mueller
 *
 */
public class AutoscrollTextPane extends JTextPane {

	/**
	 * The document this object uses to store it's data (-> text) to.
	 */
	private DefaultStyledDocument doc;
	
	/**
	 * The style used for "normal" text.
	 */
	private final static Style normalStyle;
    
	/**
	 * The style used for "warning" text.
	 */
    private final static Style errorStyle;
    
    /**
     * The style used for "criticalWarning" text.
     */
    private final static Style criticalErrorStyle;

    /**
     * The StyleContext object used to manage the used stlye on this object's document.
     */
	private static StyleContext sc;
	
	// Set up standard styles
	static {
		sc = new StyleContext();
		
		// define "normalText"
		normalStyle = sc.addStyle("normalText", null);
	    normalStyle.addAttribute(StyleConstants.Foreground, Color.black);
	    normalStyle.addAttribute(StyleConstants.FontSize, new Integer(12));
	    normalStyle.addAttribute(StyleConstants.FontFamily, "sans-serif");
	    normalStyle.addAttribute(StyleConstants.Bold, new Boolean(false));
	    
	    // define "warningText"
		errorStyle = sc.addStyle("warningText", null);
	    errorStyle.addAttribute(StyleConstants.Foreground, Color.red);
	    errorStyle.addAttribute(StyleConstants.FontSize, new Integer(12));
	    errorStyle.addAttribute(StyleConstants.FontFamily, "sans-serif");
	    errorStyle.addAttribute(StyleConstants.Bold, new Boolean(false));
	    
	    // define "criticalWarningText"
		criticalErrorStyle = sc.addStyle("warningText", null);
	    criticalErrorStyle.addAttribute(StyleConstants.Foreground, Color.red);
	    criticalErrorStyle.addAttribute(StyleConstants.FontSize, new Integer(12));
	    criticalErrorStyle.addAttribute(StyleConstants.FontFamily, "sans-serif");
	    criticalErrorStyle.addAttribute(StyleConstants.Bold, new Boolean(true));
	}
	
	/**
	 * Generated SerialVersionUID.
	 */
	private static final long serialVersionUID = -8038738138904825930L;

	/**
	 * The JScrollPane of this AutoscrollTextArea.
	 */
	private JScrollPane scrollPane = null;
	
	/**
	 * The JScrollBar of this AutoscrollTextArea.
	 */
	private JScrollBar scrollBar = null;

	
	/**
	 * Standard CTor.
	 * Using the superclasses constructor to also set a DefaultStyledDocument set up according to the needs of 
	 * the context this class was written for (providing normal-, warning- and criticalWarning-Styles
	 */
	public AutoscrollTextPane() {

		doc = new DefaultStyledDocument(sc);
		setStyledDocument(doc);
	}
	
	/**
	 * Appends a String to the contents of this TextAreas displayed Text.
	 * Additionally to the functionalities provided by JTextArea, a AutoscrollTextArea also sets the value
	 * for the connected JScrollBar to the new maximum value after the insertion
	 * Text added via this function is set as normalText 
	 * @param toAppend the String that shall be appended as normal text
	 */
	public void appendNormalText(String toAppend) {
		appendText(toAppend, normalStyle);
	}
	
	/**
	 * Appends a String to the contents of this TextAreas displayed Text.
	 * Additionally to the functionalities provided by JTextArea, a AutoscrollTextArea also sets the value
	 * for the connected JScrollBar to the new maximum value after the insertion
	 * Text added via this function is set as warningText
	 * @param toAppend the String that shall be appended as warning text
	 */
	public void appendErrorText(String toAppend) {
		appendText(toAppend, errorStyle);
	}
	
	/**
	 * Appends a String to the contents of this TextAreas displayed Text.
	 * Additionally to the functionalities provided by JTextArea, a AutoscrollTextArea also sets the value
	 * for the connected JScrollBar to the new maximum value after the insertion
	 * Text added via this function is set as criticalWarningText
	 * @param toAppend the String that shall be appended as critical warning text
	 */
	public void appendCriticalErrorText(String toAppend) {
		appendText(toAppend, criticalErrorStyle);
	}
	
	/**
	 * Helper method called by the appendXXText methods.
	 * @param toAppend the textToAppend
	 * @param styleToUse the Style to use, depending on the function used to append the Text
	 */
	private void appendText(String toAppend, Style styleToUse) {

		int offsetBeforeInsert = doc.getEndPosition().getOffset();
		try {
			doc.insertString(offsetBeforeInsert, toAppend, null);
			doc.setParagraphAttributes(offsetBeforeInsert + 1, doc.getEndPosition().getOffset(), styleToUse, false);
		} catch (BadLocationException e) {
			System.err.println("Error appending Text to document of TextPane!"); // TODO: Replace before release!
		}
		if (scrollBar != null) {
			scrollBar.setValue(scrollBar.getModel().getMaximum() + scrollBar.getModel().getExtent());
		}
	}
	
	/**
	 * Sets the JScrollPane this AutoscrollTextArea is supposed to use.
	 * @param pane the JScrollPane that should be used by this AutoscrollTextArea
	 */
	public void setScrollPane(JScrollPane pane) {
		scrollPane = pane;
		scrollBar = scrollPane.getVerticalScrollBar();
	}
	
}
