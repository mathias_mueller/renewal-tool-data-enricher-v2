package de.infinigate.renewalTool.dataEnricher.v2.gui;

import java.util.List;
import java.util.Map;

import javax.swing.JTable;

import de.infinigate.renewalTool.dataEnricher.v2.data.ApprovedChangeRec;
import de.infinigate.renewalTool.dataEnricher.v2.data.RenewalData;

/**
 * Custom JTable that offers some additional features needed here.
 * @author mathias.mueller
 *
 */
public class ChangeApprovalTable extends JTable {

	/**
	 * Generated serialVersionUID.
	 */
	private static final long serialVersionUID = 7171144483613580790L;
	
	public ChangeApprovalTable(Map<Integer, List<Map<String, String>>> changesToApprove, Map<Integer, RenewalData> renewalData) {
		super(new ChangeApprovalTableModel(changesToApprove, renewalData));
		
		ChangeApprovalTableModel tm = (ChangeApprovalTableModel) getModel();
		
		for (int i = 0; i < tm.getRowCount(); i++) {
			int noNecessaryLines = tm.getNoOfNecessaryLinesForRow(i);
			if (noNecessaryLines < 1)
				noNecessaryLines = 1;
			setRowHeight(i, noNecessaryLines * 15);
		}
		for (int currColNo : tm.getPossibleMultilineCols()) {
			getColumn(tm.getColumnName(currColNo)).setCellRenderer(new MultilineTextRenderer());
		}

	}
	
	public Map<Integer, ApprovedChangeRec> getApprovedChanges() {
		return ((ChangeApprovalTableModel) this.getModel()).getApprovedChanges();
	}

}
