package de.infinigate.renewalTool.dataEnricher.v2.gui;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.swing.table.AbstractTableModel;

import com.google.common.primitives.Ints;

import de.infinigate.renewalTool.dataEnricher.v2.data.ApprovedChangeRec;
import de.infinigate.renewalTool.dataEnricher.v2.data.RenewalData;

/**
 * Custom table model that holds the data for the AmbiguousEmailTable.
 * @author mathias.mueller
 */
public class ChangeApprovalTableModel extends AbstractTableModel {

	/**
	 * Generated serialVersionUID.
	 */
	private static final long serialVersionUID = -4729820072821767697L;

	/**
	 * The column names that should be used.
	 */
	public String[] columnNames = null;
	
	/**
	 * The data.
	 */
	private Map<Integer, List<Map<String, String>>> changesToApprove;
	
	/**
	 * RenewalData to further explain changes.
	 */
	private Map<Integer, RenewalData> renewalData;
		
	/**
	 * List of columns that must be editable in the table (those with "new - ").
	 */
	private int[] editableCols;
	
	/**
	 * List of columns that can have multiple lines (those with "suggested - ").
	 */
	private int[] possibleMultilineCols;

	/**
	 * A mapping which RenewalData.Id is to be displayed in each table row.
	 */
	private Integer[] rowToRenewalIdMapping;

	/**
	 * The number of "standard columns" (meaning columns that are for general information like the bool, id, (reseller name)).
	 */
	private int standardCols;
	
	/**
	 * The columns for which new values have been suggested and are to be approved in this table.
	 */
	private List<String> columnsToApprove = new ArrayList<String>();
	
	/**
	 * Data on the changes and their approval status.
	 * Filled from the changesToApprove Map by default, edited in table
	 */
	private Map<Integer, ApprovedChangeRec> approvedChanges;
	
	/**
	 * Build a ChangeApprovalTableModel.
	 * @param changesToApprove the changes to approve
	 * @param renewalData the data to change
	 */
	public ChangeApprovalTableModel(Map<Integer, List<Map<String, String>>> changesToApprove, Map<Integer, RenewalData> renewalData) {
		this.changesToApprove = changesToApprove;
		this.renewalData = renewalData;
		columnNames = buildColumnNameArray();
		rowToRenewalIdMapping = buildRowToRenewalIdMapping();
		approvedChanges = buildDefaultChanges();
	}
	
	/**
	 * Take the first of all suggested changes an write them as default-values for the changes to do
	 * @return
	 */
	private Map<Integer, ApprovedChangeRec> buildDefaultChanges() {

		Map<Integer, ApprovedChangeRec> retValUnderConstruction = new HashMap<Integer, ApprovedChangeRec>();
		
		for (Entry<Integer, List<Map<String, String>>> currEntry : changesToApprove.entrySet()) {
			ApprovedChangeRec tempApprovedChangeRec = new ApprovedChangeRec(currEntry.getKey());
			Map<String, String> tempAppChRecMap = tempApprovedChangeRec.getChanges();
			for (Map<String, String> currMap : currEntry.getValue()) {
				for (Entry<String, String> currMapEntry : currMap.entrySet()) {
						if (tempAppChRecMap.get(currMapEntry.getKey()) == null)
							tempAppChRecMap.put(currMapEntry.getKey(), currMapEntry.getValue());
				}
			}
			retValUnderConstruction.put(currEntry.getKey(), tempApprovedChangeRec);
		}

		return retValUnderConstruction;
	}

	/**
	 * Builds a mapping "table row" <-> "renewaldata id".
	 * @return the built mapping
	 */
	private Integer[] buildRowToRenewalIdMapping() {
		
		List<Integer> mappingUnderConstruction = new ArrayList<Integer>();
		
		for (Entry<Integer, List<Map<String, String>>> currChangeEntry : changesToApprove.entrySet()) {
			mappingUnderConstruction.add(currChangeEntry.getKey());
		}

		return mappingUnderConstruction.toArray(new Integer[1]);
	}

	/**
	 * Dynamically generates the column names of this table depending on what is to be approved.
	 * @return the built column names
	 */
	private String[] buildColumnNameArray() {

		List<String> builtColNames = new ArrayList<String>();
		List<String> colsToApprove = new ArrayList<String>();
		Set<String> uniqueNamesColsToApprove = new HashSet<String>();
		
		// Collect all different cols for that changes must be approved
		for (List<Map<String, String>> currList : changesToApprove.values()) {
			for (Map<String, String> currMap : currList) {				
				for (String currKey : currMap.keySet()) {
					uniqueNamesColsToApprove.add(currKey);
				}				
			}			
		}
		
//		editableCols = new int[uniqueNamesColsToApprove.size()];
//		possibleMultilineCols = new int[uniqueNamesColsToApprove.size()];
		List<Integer> editableColList = new ArrayList<Integer>();
		List<Integer> multilineColList = new ArrayList<Integer>();
		
		int colCount = 0;
		
		// build actual colNames.
		// general lines
		builtColNames.add("<html>Apply<br />suggested<br />change?<br /></html>");
		colCount++;
		builtColNames.add("<html>Id (as in<br />Renewal<br/Worksheet)<br /></html>");
		colCount++;
		if (!uniqueNamesColsToApprove.contains("Reseller Name")) { 
			builtColNames.add("<html>Reseller<br />Name<br /></html>");
			colCount++;
		}
		standardCols = colCount;
		
		// lines for each suggested enrichment
		for (String uniqueKey : uniqueNamesColsToApprove) {
			builtColNames.add("<html>Original values<br />for \"" + uniqueKey + "\"<br>&nbsp;</html>");
			builtColNames.add("<html>Suggestions for<br />\"" + uniqueKey + "\"<br>&nbsp;</html>");
			builtColNames.add("<html>Value for<br />\"" + uniqueKey + "\"<br>&nbsp;</html>");
			columnsToApprove.add(uniqueKey);
			colCount += 3;
			editableColList.add(colCount - 1); // -1 to make the "number" an "index"
			multilineColList.add(colCount - 1 - 1); // -1 to make the "number" an "index"
//			editableCols[(colCount - standardCols) / 3] = colCount;
		}

		editableCols = Ints.toArray(editableColList);
		Arrays.sort(editableCols);
		possibleMultilineCols = Ints.toArray(multilineColList);
		return builtColNames.toArray(new String[1]);
	}

	/**
	 * Retrieves the total number of columns.
	 */
	public int getColumnCount() {

		return columnNames.length; 
	}

	/**
	 * Retrieves the total number of data-rows.
	 */
	public int getRowCount() {
		
		return changesToApprove.keySet().size();
		
	}
	
	/**
	 * Returns the java-class for the data in the column specified by the parameter.
	 * @param col the column in question
	 */
	public Class<?> getColumnClass(int col) {
		try {
			switch (col) {
			
			case 0:
				return Class.forName("java.lang.Boolean");
				
			default:
				return Class.forName("java.lang.String");

			}
		} catch (ClassNotFoundException e) {
			System.err.println("Critical internal error. Something is very wrong with Java.");
		}
		
		return "".getClass();
	}
	
	/**
	 * Returns the name of the column with the specified index.
	 */
	public String getColumnName(int col) {
		return columnNames[col];
	}
	
	/**
	 * Returns if the cell specified by the parameters is editable.
	 * @param row the row of the cell in question
	 * @param col the column of the cell in question
	 */
	public boolean isCellEditable(int row, int col) {
		if (col == 0)
			return true;
		
		if (Arrays.binarySearch(editableCols, col) >= 0) 
			return true;
		
		return false;
	}

	/**
	 * Returns the value of the cell specified by the parameters.
	 * @param row the row of the cell in question
	 * @param col the column of the cell in question
	 * @return the value
	 */
	public Object getValueAt(int row, int col) {
		
		if (col == 0) {
			return approvedChanges.get(rowToRenewalIdMapping[row]).isApplyChange();
		} else if (col == 1) {
			return rowToRenewalIdMapping[row];
		} else if (col == 2 && standardCols == 3) {
			return renewalData.get(rowToRenewalIdMapping[row]).getResellerName();
		} else if (col >= 2) {
			int colIndexWOStandardCols = col - standardCols;
			int columnType = colIndexWOStandardCols % 3; // 0 = old, 1 = suggestions, 2 = new 
			int columnsToApproveId = colIndexWOStandardCols / 3; // index in columnsToApprove List for fieldname that is enriched
			switch (columnType) {
			case 0:
				return renewalData.get(rowToRenewalIdMapping[row]).getFieldValByName(columnsToApprove.get(columnsToApproveId));
				
			case 1:
				StringBuilder sb = new StringBuilder();
				String nameOfCurrCol = columnsToApprove.get(columnsToApproveId);
				List<Map<String, String>> listOfPossibleCandidates = changesToApprove.get(rowToRenewalIdMapping[row]);
				for (Map<String, String> currMap : listOfPossibleCandidates) {
					if (currMap.get(nameOfCurrCol) != null) {
						sb.append(currMap.get(nameOfCurrCol));
						sb.append("\n");
					}
				}
				return sb.toString();
				
			case 2: 
				return approvedChanges.get(rowToRenewalIdMapping[row]).getChanges().get(columnsToApprove.get(columnsToApproveId));
			}
		}
			
		return "Value couldn't be retrieved";
	}
	
	/**
	 * Sets the value of the specified cell to the value given.
	 * @param aValue the value to set	 
	 * @param row the row of the cell in question
	 * @param col the column of the cell in question
	 */
	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
		
		if (columnIndex == 0) {
			Boolean objValue = (Boolean) aValue;
			approvedChanges.get(rowToRenewalIdMapping[rowIndex]).setApplyChange(objValue);
		} else if (columnIndex > 0) {
			int noOfColumn = Arrays.binarySearch(editableCols, columnIndex);
			approvedChanges.get(rowToRenewalIdMapping[rowIndex]).addChange(columnsToApprove.get(noOfColumn), (String) aValue);
		} else {
			System.err.println("Field that is not editable should have been updated.");
		}
		
	}
	
	/**
	 * Returns the number of lines that the table row should have (= the number of different email-addresses in the field "E-Mails in Input-file" of data).
	 * @param row the row in question 
	 * @return the number of different values in the "suggestions..." column and therefore the number of lines this row should have
	 */
	public int getNoOfNecessaryLinesForRow(int row) {
		return changesToApprove.get(rowToRenewalIdMapping[row]).size();
	}

	/**
	 * @return the approvedChanges
	 */
	public Map<Integer, ApprovedChangeRec> getApprovedChanges() {
		return approvedChanges;
	}

	/**
	 * @return the possibleMultilineCols
	 */
	public int[] getPossibleMultilineCols() {
		return possibleMultilineCols;
	}

}
