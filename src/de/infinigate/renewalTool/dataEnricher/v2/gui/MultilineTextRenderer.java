package de.infinigate.renewalTool.dataEnricher.v2.gui;

import java.awt.Component;

import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;

public class MultilineTextRenderer extends JTextArea implements
		TableCellRenderer {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2712086679705936548L;
	
	private final DefaultTableCellRenderer adaptee = new DefaultTableCellRenderer();
	
	public MultilineTextRenderer() {
		super();
		setLineWrap(true);
		setWrapStyleWord(true);
	}
	
	public Component getTableCellRendererComponent(JTable table, Object object,
        boolean isSelected, boolean hasFocus,
        int row, int column) {
        adaptee.getTableCellRendererComponent(table, object,
                isSelected, hasFocus, row, column);
        setForeground(adaptee.getForeground());
        setBackground(adaptee.getBackground());
        setBorder(adaptee.getBorder());
        setFont(adaptee.getFont());
        setText(adaptee.getText());
		
		return this;
	}

}
