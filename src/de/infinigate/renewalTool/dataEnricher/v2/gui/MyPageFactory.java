package de.infinigate.renewalTool.dataEnricher.v2.gui;

import java.util.ArrayList;
import java.util.List;

import org.ciscavate.cjwizard.PageFactory;
import org.ciscavate.cjwizard.WizardPage;
import org.ciscavate.cjwizard.WizardSettings;

import de.infinigate.renewalTool.dataEnricher.v2.wizardWindows.AmbiguousEmailApplication;
import de.infinigate.renewalTool.dataEnricher.v2.wizardWindows.AmbiguousEmailProcessing;
import de.infinigate.renewalTool.dataEnricher.v2.wizardWindows.AmbiguousEmailWorking;
import de.infinigate.renewalTool.dataEnricher.v2.wizardWindows.ChangeApplication;
import de.infinigate.renewalTool.dataEnricher.v2.wizardWindows.ChangeApproval;
import de.infinigate.renewalTool.dataEnricher.v2.wizardWindows.Done;
import de.infinigate.renewalTool.dataEnricher.v2.wizardWindows.OptionSelection;
import de.infinigate.renewalTool.dataEnricher.v2.wizardWindows.RenewalProfileSelection;
import de.infinigate.renewalTool.dataEnricher.v2.wizardWindows.ReuploadData;
import de.infinigate.renewalTool.dataEnricher.v2.wizardWindows.Welcome;
import de.infinigate.renewalTool.dataEnricher.v2.wizardWindows.Working;
import de.infinigate.renewalTool.dataEnricher.v2.workers.ResellerNameEnricherWorker;

/**
 * This programs implementation of a PageFactory.
 * Supplies the Pages that are used within the wizard
 * @author mathias.mueller
 */
public class MyPageFactory implements PageFactory {

	/**
	 * The WizardPages managed by this PageFactory.
	 * Is modified according to selected enrichment options in method createPage
	 */
	private WizardPage[] pages = {
			new Welcome(),
			new RenewalProfileSelection(),
			new OptionSelection()
	};

	   /**
	    * Retrieves a wizard page based on the path of pages covered
	    * so far between now and the start of the dialog, and the map of settings.
	    * @param path  The list of all WizardPages seen so far.
	    * @param settings The Map of settings collected.
	    * @return The next page 
	    */
	public WizardPage createPage(List<WizardPage> path, WizardSettings settings) {
		if (path.size() == 3) { // option selection passed
			boolean resellerNameEnrichment = (Boolean) settings.get("resellerNameEnrichment");
			boolean ambiguousMailCheck = (Boolean) settings.get("ambiguousEmailCheck");
			List<WizardPage> pagesToBuild = new ArrayList<WizardPage>();
			pagesToBuild.add(pages[0]);
			pagesToBuild.add(pages[1]);
			pagesToBuild.add(pages[2]);
			if (resellerNameEnrichment) {
				pagesToBuild.add(new Working(Integer.toString(pagesToBuild.size()), "Enriching \"Reseller Name\"", new ResellerNameEnricherWorker()));
				pagesToBuild.add(new ChangeApproval(Integer.toString(pagesToBuild.size()), "Approve \"Reseller Name\" changes", 
						ResellerNameEnricherWorker.SETTINGS_SAVE_NAME));
				pagesToBuild.add(new ChangeApplication(Integer.toString(pagesToBuild.size()), "Apply \"Reseller Name\" changes", 
						ResellerNameEnricherWorker.SETTINGS_SAVE_NAME));
			}
			if (ambiguousMailCheck) {
				pagesToBuild.add(new AmbiguousEmailWorking(Integer.toString(pagesToBuild.size())));
				pagesToBuild.add(new AmbiguousEmailProcessing(Integer.toString(pagesToBuild.size())));
				pagesToBuild.add(new AmbiguousEmailApplication(Integer.toString(pagesToBuild.size())));
			}
			pagesToBuild.add(new ReuploadData(Integer.toString(pagesToBuild.size())));
			pagesToBuild.add(new Done(Integer.toString(pagesToBuild.size())));
			
			pages = pagesToBuild.toArray(new WizardPage[1]);
		}
		if (path.size() > 3) {
			if (path.get(path.size() - 1) instanceof Working) { // Hide remaining two steps when no suggestions for change can be made.
				Working lastWorkingWindow = (Working) path.get(path.size() - 1);
				if (!lastWorkingWindow.getSuggestedChangesFound()) {
					ArrayList<WizardPage> tempList = new ArrayList<WizardPage>();
					for (WizardPage currP : pages) {
						tempList.add(currP);
					}
					tempList.remove(path.size() + 1);
					tempList.remove(path.size());
					pages = tempList.toArray(new WizardPage[1]);
				}
			}
			if (path.get(path.size() - 1) instanceof ChangeApproval) { // Read output from general "ChangeApproval" classes.
				ChangeApproval ca = (ChangeApproval) path.get(path.size() - 1);
				settings.put(ca.getNameOfChanges(), ca.getApprovedChanges());
			}
			if (path.get(path.size() - 1) instanceof AmbiguousEmailWorking) { // Remove AmbiguousEmailHandling windows if no ambiguous email data was found.
				Boolean ambiguousDataPresentFlag = (Boolean) settings.get(AmbiguousEmailWorking.AMBIGUOUS_EMAIL_PRESENT_FLAG);
				if (ambiguousDataPresentFlag == null || ambiguousDataPresentFlag == false) {
					ArrayList<WizardPage> tempList = new ArrayList<WizardPage>();
					for (WizardPage currP : pages) {
						tempList.add(currP);
					}
					tempList.remove(path.size() + 1);
					tempList.remove(path.size());
					pages = tempList.toArray(new WizardPage[1]);
				}
			}
			if (path.get(path.size() - 1) instanceof AmbiguousEmailProcessing) { // Read output from special "AmbiguousEmailProcessing" class.
				AmbiguousEmailProcessing aep = (AmbiguousEmailProcessing) path.get(path.size() - 1);
				settings.put(AmbiguousEmailApplication.AMBIGUOUS_EMAIL_SETTINGS_KEY, aep.getData());
			}
		}
		
		WizardPage page = pages[path.size()];
		return page;
	}

}
