package de.infinigate.renewalTool.dataEnricher.v2.gui;

import java.awt.Dimension;
import java.awt.Image;
import java.awt.Toolkit;
import java.net.URL;
import java.util.List;

import javax.swing.JDialog;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import org.ciscavate.cjwizard.PageFactory;
import org.ciscavate.cjwizard.WizardContainer;
import org.ciscavate.cjwizard.WizardListener;
import org.ciscavate.cjwizard.WizardPage;
import org.ciscavate.cjwizard.WizardSettings;
import org.ciscavate.cjwizard.pagetemplates.PageTemplate;
import org.ciscavate.cjwizard.pagetemplates.TitledPageTemplate;

/**
 * The Representation of this programs wizard.
 * @author mathias.mueller
 */
public class Wizard extends JDialog {

	/**
	 * Generated Serial Version UID.
	 */
	private static final long serialVersionUID = -4522581150116012445L;
	
	/**
	 * The PageFactory.
	 */
	public PageFactory pf;

	/**
	 * Main-Method.
	 * @param args no args expected
	 */
	public static void main(String[] args) {
		Wizard wiz = new Wizard();
		wiz.setVisible(true);
	}
	
	/**
	 * Standard CTor, builds this Wizard.
	 */
	public Wizard() {
		
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (UnsupportedLookAndFeelException e) {
			e.printStackTrace();
		}
		
		PageFactory pf = new MyPageFactory();
		PageTemplate pt = new TitledPageTemplate();
		WizardContainer wc = new WizardContainer(pf, pt);
		Wizard.this.setTitle("Infinigate Renewal Data Enricher");
		URL url = ClassLoader.getSystemResource("Infinigate.png");
		Image img = Toolkit.getDefaultToolkit().createImage(url);
		Wizard.this.setIconImage(img);
	    
		wc.addWizardListener(new WizardListener() {

			public void onCanceled(List<WizardPage> path, WizardSettings settings) {
				Wizard.this.dispose();
			}

			public void onFinished(List<WizardPage> path, WizardSettings settings) {
				Wizard.this.dispose();
			}

			public void onPageChanged(WizardPage newPage, List<WizardPage> path) {
				
			}
		});
		
		this.setPreferredSize(new Dimension(450,375));
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		this.getContentPane().add(wc);
		this.pack();
	}
	
}
