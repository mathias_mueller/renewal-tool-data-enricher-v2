package de.infinigate.renewalTool.dataEnricher.v2.wizardWindows;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingWorker;

import org.ciscavate.cjwizard.WizardPage;
import org.ciscavate.cjwizard.WizardSettings;

import de.infinigate.renewalTool.dataEnricher.v2.common.XmlConfigManager;
import de.infinigate.renewalTool.dataEnricher.v2.data.AmbiguousEmailData;
import de.infinigate.renewalTool.dataEnricher.v2.data.RenewalData;
import de.infinigate.renewalTool.dataEnricher.v2.data.ResellerRuleData;
import de.infinigate.renewalTool.dataEnricher.v2.gui.AutoscrollTextPane;
import de.infinigate.renewalTool.dataEnricher.v2.workers.ResellerRuleApplier;
import de.infinigate.renewalTool.dataEnricher.v2.workers.SQLResellerRuleUploader;

public class AmbiguousEmailApplication extends WizardPage implements PropertyChangeListener {
	
	/**
	 * Generated serialVersionUID.
	 */
	private static final long serialVersionUID = -1652585425681665794L;

	/**
	 * Name used to obtain the ambiguousEmailData when starting processing.
	 */
	public static final String AMBIGUOUS_EMAIL_SETTINGS_KEY = "ambiguousEmailData";
	
	/**
	 * The progressBar for change application.
	 */
	private final JProgressBar pb_applyChanges = new JProgressBar();
	
	/**
	 * This window's textArea for status messages.
	 */
	private final AutoscrollTextPane textArea;

	/**
	 * The name of the input profile.
	 */
	private String importProfileName;

	/**
	 * Map that is used to buffer Reseller Rules that need to be created, applied and uploaded to the Db.
	 */
	private Map<String, ResellerRuleData> resellerRules;

	/**
	 * Local reference to the xmlConfig object of this program.
	 */
	private XmlConfigManager locXmlConfig;

	/**
	 * Ref to the ResellerRuleApplier reference of this object. 
	 */
	private ResellerRuleApplier rra;
	
	/**
	 * Create the page.
	 * @param the wizardStep this Page is to be used as
	 */
	public AmbiguousEmailApplication(String wizardStepNo) {

		super(wizardStepNo, "Applying changes from last step.");
		
		JLabel lblParsingInputfile = new JLabel("Applying approved changes from last step...");
		getPb_applyChanges().setStringPainted(true);
		textArea = new AutoscrollTextPane();
		getTextArea().setEditable(false);
		
		JScrollPane text_sp = new JScrollPane(getTextArea());
		getTextArea().setScrollPane(text_sp);
		text_sp.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		
		JLabel lblLog = new JLabel("Status:");

		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(Alignment.TRAILING, groupLayout.createSequentialGroup()
							.addContainerGap()
							.addComponent(text_sp, GroupLayout.DEFAULT_SIZE, 430, Short.MAX_VALUE))
						.addGroup(groupLayout.createSequentialGroup()
							.addContainerGap()
							.addComponent(lblLog))
						.addGroup(groupLayout.createSequentialGroup()
							.addContainerGap()
							.addComponent(pb_applyChanges, GroupLayout.DEFAULT_SIZE, 430, Short.MAX_VALUE))
						.addGroup(groupLayout.createSequentialGroup()
							.addContainerGap()
							.addComponent(lblParsingInputfile)))
					.addContainerGap())
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblParsingInputfile)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(pb_applyChanges, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblLog)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(text_sp, GroupLayout.DEFAULT_SIZE, 210, Short.MAX_VALUE)
					.addContainerGap())
		);
		setLayout(groupLayout);
	}

	/**
	 * Method called during the rendering of this WizardPage.
	 * Used for necessary initializations on the page and to start the process
	 */
	public void rendering(List<WizardPage> path, WizardSettings settings) {
		
		setNextEnabled(false);
		AmbiguousEmailData[] ambiguousEmailData = (AmbiguousEmailData[]) settings.get(AMBIGUOUS_EMAIL_SETTINGS_KEY);
		Map<Integer, RenewalData> renewalData = (Map<Integer, RenewalData>) settings.get("renewalDataIndexed");
		importProfileName = (String) settings.get(RenewalProfileSelection.RENEWAL_PROFILE_SETTINGS_KEY);
		resellerRules = new HashMap<String, ResellerRuleData>();
		locXmlConfig = (XmlConfigManager) settings.get("xmlConfig"); 
		
		for (AmbiguousEmailData currEntry : ambiguousEmailData) {
			if (currEntry.isApply())
				resellerRules.put(currEntry.getNameForRule(), new ResellerRuleData(currEntry.getNameForRule(), currEntry.getEmailForRule(), "", "", "", "", true));
		}
		
		rra = new ResellerRuleApplier();
		rra.addPropertyChangeListener(this);
		rra.setData(renewalData);
		rra.setConfig(resellerRules);
		rra.execute();
		
	}

	/**
	 * Method that is called on propertyChange when this class is registered as listener.
	 * @param evt the PropertyChangeEvent
	 */
	public void propertyChange(PropertyChangeEvent evt) {
		if ("progress".equals(evt.getPropertyName())) {
			if (evt.getSource() instanceof ResellerRuleApplier) {
				pb_applyChanges.setValue(((Integer) evt.getNewValue()) / 2);
			} else if (evt.getSource() instanceof SQLResellerRuleUploader) {
				pb_applyChanges.setValue((((Integer) evt.getNewValue()) / 2) + 50);
			}
		} else if ("state".equals(evt.getPropertyName())) {
			if (evt.getSource() instanceof ResellerRuleApplier) {
				if (evt.getNewValue() == SwingWorker.StateValue.STARTED) {
					textArea.appendNormalText("Starting to apply newly defined Reseller Rules...\n");
				} else if (evt.getNewValue() == SwingWorker.StateValue.DONE) {
					textArea.appendNormalText("Done applying new Reseller Rules.\n");
					SQLResellerRuleUploader rru = new SQLResellerRuleUploader();
					rru.addPropertyChangeListener(this);
					rru.setImportProfileCode(importProfileName);
					rru.setRulesToUpload(resellerRules);
					rru.setXmlConfig(locXmlConfig);
					rru.execute();
				}
			} else if (evt.getSource() instanceof SQLResellerRuleUploader) {
				if (evt.getNewValue() == SwingWorker.StateValue.STARTED) {
					textArea.appendNormalText("____________________________________" + "\n" + "Starting to upload new Reseller Rules to DB...\n");
				} else if (evt.getNewValue() == SwingWorker.StateValue.DONE) {
					textArea.appendNormalText("Upload complete.\n");
					setNextEnabled(true);
				}
			}
		}
	}
	
	/**
	 * @return the textArea
	 */
	public AutoscrollTextPane getTextArea() {
		return textArea;
	}

	/**
	 * @return the pb_applyChanges
	 */
	public JProgressBar getPb_applyChanges() {
		return pb_applyChanges;
	}

}
