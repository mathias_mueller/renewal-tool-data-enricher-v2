package de.infinigate.renewalTool.dataEnricher.v2.wizardWindows;

import org.ciscavate.cjwizard.WizardPage;
import org.ciscavate.cjwizard.WizardSettings;

import de.infinigate.renewalTool.dataEnricher.v2.data.AmbiguousEmailData;
import de.infinigate.renewalTool.dataEnricher.v2.gui.AmbiguousEmailTable;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

public class AmbiguousEmailProcessing extends WizardPage {

	/**
	 * Generated serialVersionUID.
	 */
	private static final long serialVersionUID = 2144232158372898438L;

	/**
	 * The table used in this page.
	 */
	private static AmbiguousEmailTable table;
	
	/**
	 * Flag indicating, whether this WizardPage was already rendered.
	 */
	private boolean rendered;
	
	/**
	 * The data to display in the table.
	 */
	private AmbiguousEmailData[] data;
	
	/**
	 * Create the page.
	 * @param noWizardStep the step in the wizard that this WizardPage should be used for
	 */
	public AmbiguousEmailProcessing(String noWizardStep) {
		super(noWizardStep,"Process ambiguous emails.");
		setLayout(new GridBagLayout());
		rendered = false;
	}
	
	/**
	 * Method called when rendering this page.
	 * @param path the path through the different WizardPages so far
	 * @param settings the reference to this program's WizardSettings object
	 */
	public void rendering(List<WizardPage> path, WizardSettings settings) {

		super.rendering(path, settings);
		data = (AmbiguousEmailData[]) settings.get(AmbiguousEmailWorking.AMBIGUOUS_EMAIL_DATA_SETTINGSKEY);
		if (!rendered) {
			rendered = true;
			if (data != null && data.length > 0 && data[0] != null) { 
				
				GridBagConstraints c_table = new GridBagConstraints();
				c_table.gridx = 0;
				c_table.gridy = 0;
				c_table.weightx = 1;
				c_table.weighty = 1;
				c_table.fill = GridBagConstraints.BOTH;
				
				table = new AmbiguousEmailTable(data);
				table.setPreferredScrollableViewportSize(new Dimension(500,100));
				table.setFillsViewportHeight(true);
				
				JScrollPane table_sp = new JScrollPane(table);
				add(table_sp, c_table);
				
				JPanel btn_pane = new JPanel();
				
				JButton btn_selectAll = new JButton("Select all");		
				ActionListener al_all = new ActionListener() {

					public void actionPerformed(ActionEvent arg0) {

						for (AmbiguousEmailData currRec : table.getData()) {
							currRec.setApply(true);
						}
						table.updateUI();
						
					}
					
				};
				btn_selectAll.addActionListener(al_all);
				
				JButton btn_selectNone = new JButton("Select none");		
				ActionListener al_none = new ActionListener() {

					public void actionPerformed(ActionEvent arg0) {
						
						for (AmbiguousEmailData currRec : table.getData()) {
							currRec.setApply(false);
						}
						table.updateUI();
						
					}
					
				};
				btn_selectNone.addActionListener(al_none);
				
				btn_pane.setLayout(new GridLayout(0,2));
				btn_pane.add(btn_selectAll);
				btn_pane.add(btn_selectNone);
				GridBagConstraints c_pane = new GridBagConstraints();
				c_pane.gridx = 0;
				c_pane.gridy = 1;
				c_pane.weightx = 0;
				c_pane.weighty = 0;
				c_pane.anchor = GridBagConstraints.WEST;
				add(btn_pane, c_pane);
			} 
		}
		setNextEnabled(true);
	}

	/**
	 * @return the table
	 */
	public AmbiguousEmailData[] getData() {
		return data;
	}

}