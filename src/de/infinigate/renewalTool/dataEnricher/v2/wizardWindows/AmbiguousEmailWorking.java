package de.infinigate.renewalTool.dataEnricher.v2.wizardWindows;

import java.awt.Font;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.List;
import java.util.Map;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.SwingWorker;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;

import org.ciscavate.cjwizard.WizardPage;
import org.ciscavate.cjwizard.WizardSettings;

import de.infinigate.renewalTool.dataEnricher.v2.common.XmlConfigManager;
import de.infinigate.renewalTool.dataEnricher.v2.data.RenewalData;
import de.infinigate.renewalTool.dataEnricher.v2.gui.AutoscrollTextPane;
import de.infinigate.renewalTool.dataEnricher.v2.workers.AbstractDataEnricherWorker;
import de.infinigate.renewalTool.dataEnricher.v2.workers.AmbiguousEmailWorker;
import de.infinigate.renewalTool.dataEnricher.v2.workers.RenewalDataObtainer;

/**
 * Special Class to fulfill the job of the general "Working"-class for the legacy code of the ambiguous E-Mail check.
 * @author mathias.mueller
 */
public class AmbiguousEmailWorking extends WizardPage implements PropertyChangeListener {

	/**
	 * Generated serialVersionUID.
	 */
	private static final long serialVersionUID = 2493176137815124000L;

	/**
	 * The key under which AmbiguousEmailData is put in the settings-object.
	 */
	public static final String AMBIGUOUS_EMAIL_DATA_SETTINGSKEY = "ambiguousEmailData";
	
	/**
	 * The key under which the flag whether ambiguous email data was found is stored in the setting-object.
	 */
	public static final String AMBIGUOUS_EMAIL_PRESENT_FLAG = "ambiguousEmailDataPresent";

	/**
	 * Local reference to the WizardSetting object of this program.
	 */
	private WizardSettings locSettings;
	
	/**
	 * Local reference to this program's XmlConfigManager.
	 */
	private XmlConfigManager xmlConfig;

	/**
	 * The progressBar for input file parsing.
	 */
	private final JProgressBar pb_working = new JProgressBar();
	
	/**
	 * This window's textArea for status messages.
	 */
	private final AutoscrollTextPane textArea;
	
	/**
	 * The SwingWorker used to obtain data from the DB if necessary..
	 */
	private RenewalDataObtainer rdo;
	
	/**
	 * The SwingWorker
	 */
	private AmbiguousEmailWorker aew;
	
	/**
	 * Create the WizardPage.
	 */
	public AmbiguousEmailWorking(String wizardStepNo) {

		super(wizardStepNo, "Searching for ambiguous E-Mails for a single \"Reseller Name\"...");
		JLabel lblParsingInputfile = new JLabel("Working...");
		pb_working.setStringPainted(true);
		textArea = new AutoscrollTextPane();
		getTextArea().setEditable(false);
		
		JScrollPane text_sp = new JScrollPane(getTextArea());
		getTextArea().setScrollPane(text_sp);
		text_sp.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		
		JLabel lblLog = new JLabel("Status:");

		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(Alignment.TRAILING, groupLayout.createSequentialGroup()
							.addContainerGap()
							.addComponent(text_sp, GroupLayout.DEFAULT_SIZE, 430, Short.MAX_VALUE))
						.addGroup(groupLayout.createSequentialGroup()
							.addContainerGap()
							.addComponent(lblLog))
						.addGroup(groupLayout.createSequentialGroup()
							.addContainerGap()
							.addComponent(pb_working, GroupLayout.DEFAULT_SIZE, 430, Short.MAX_VALUE))
						.addGroup(groupLayout.createSequentialGroup()
							.addContainerGap()
							.addComponent(lblParsingInputfile)))
					.addContainerGap())
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblParsingInputfile)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(pb_working, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblLog)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(text_sp, GroupLayout.DEFAULT_SIZE, 210, Short.MAX_VALUE)
					.addContainerGap())
		);
		setLayout(groupLayout);
	}

	/**
	 * Method called during the rendering of this WizardPage.
	 * Used for necessary initializations on the page
	 */
	public void rendering(List<WizardPage> path, WizardSettings settings) {
		
		super.rendering(path, settings);
		setPrevEnabled(false);
		setNextEnabled(false);
		
		locSettings = settings;
		
		Map<Integer, RenewalData> renewalDataIndexed = (Map<Integer, RenewalData>) settings.get("renewalDataIndexed");
		xmlConfig = (XmlConfigManager) settings.get("xmlConfig");
		
		if (renewalDataIndexed == null) {
			// obtain data from DB, none present in settings.
			textArea.appendNormalText("First enrichment process, data needs to be obtained from DB...");
			String importProfileCode = (String) settings.get("renewalProfile");
			
			if (importProfileCode == null || xmlConfig == null) { 
				JOptionPane.showMessageDialog(null,"Error obtaining RenewalData", "\n ERROR: Couldn't obtain importProfileCode and/or xmlConfig when trying to read data.", JOptionPane.ERROR_MESSAGE);
			} else {
				rdo = new RenewalDataObtainer(xmlConfig, importProfileCode);
				rdo.addPropertyChangeListener(this);
				rdo.execute();
			}
		} else {
			textArea.appendNormalText("Data was already obtained from DB in an earlier step, continuing.\n");
			aew = new AmbiguousEmailWorker(renewalDataIndexed);
			aew.addPropertyChangeListener(this);
			aew.execute();
		}
		
	}
	
	/**
	 * Method that is called on property change.
	 * @param evt the PropertyChangeEvent to handle
	 */
	public void propertyChange(PropertyChangeEvent evt) {
		if ("progress".equals(evt.getPropertyName())) {
			if (evt.getSource() instanceof AmbiguousEmailWorker) {
				pb_working.setValue((Integer) evt.getNewValue());
			}

		} else if ("state".equals(evt.getPropertyName())) {
			if (evt.getSource() instanceof RenewalDataObtainer) {
				if (evt.getNewValue() == SwingWorker.StateValue.DONE) {
					textArea.appendNormalText("done.\n");
					locSettings.put("renewalDataIndexed", rdo.getOutputIndexed());
					aew = new AmbiguousEmailWorker(rdo.getOutputIndexed());
					aew.addPropertyChangeListener(this);
					aew.execute();
				}
			} else if (evt.getSource() instanceof AmbiguousEmailWorker) {
				if (evt.getNewValue() == SwingWorker.StateValue.STARTED) {
					textArea.appendNormalText("____________________________________" + "\nSearching for ambiguous E-Mail TOs for a "
							+ "single \"Reseller Name\"...\n");
				} else if (evt.getNewValue() == SwingWorker.StateValue.DONE) {
					textArea.appendNormalText("Searching for ambiguous E-Mail TOs completed. Result: " 
							+ (aew.isDataWithAmbiguousEmailsPresent() ? "ambiguous data found." : "NO ambiguous data found."));
					locSettings.put(AMBIGUOUS_EMAIL_PRESENT_FLAG, aew.isDataWithAmbiguousEmailsPresent());
					locSettings.put(AMBIGUOUS_EMAIL_DATA_SETTINGSKEY, aew.getAmbiguousEmailData());
					setNextEnabled(true);
				}
			}
		}
		
	}

	/**
	 * @return the textArea
	 */
	public AutoscrollTextPane getTextArea() {
		return textArea;
	}

}
