package de.infinigate.renewalTool.dataEnricher.v2.wizardWindows;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.List;
import java.util.Map;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingWorker;

import org.ciscavate.cjwizard.WizardPage;
import org.ciscavate.cjwizard.WizardSettings;

import de.infinigate.renewalTool.dataEnricher.v2.data.ApprovedChangeRec;
import de.infinigate.renewalTool.dataEnricher.v2.data.RenewalData;
import de.infinigate.renewalTool.dataEnricher.v2.gui.AutoscrollTextPane;
import de.infinigate.renewalTool.dataEnricher.v2.workers.ChangeApplier;

public class ChangeApplication extends WizardPage implements PropertyChangeListener {

	/**
	 * Generated serialVersionUID.
	 */
	private static final long serialVersionUID = 8432249260415687267L;

	/**
	 * The progressBar for change application.
	 */
	private final JProgressBar pb_applyChanges = new JProgressBar();
	
	/**
	 * This window's textArea for status messages.
	 */
	private final AutoscrollTextPane textArea;
	
	/**
	 * Name of the entry in the WizardSettings under which the changes that should be applied by this Page are stored.
	 */
	private String keyChangesToApply;

	/**
	 * Reference to the ChangeApplier Object used to apply the changes.
	 */
	private ChangeApplier ca;

	/**
	 * Local reference to the WizardSettings object used by this program.
	 */
	private WizardSettings locSettings;
	
	/**
	 * Create the ChangeApplication window.
	 * @param wizardStepNo the step in the wizard this instance is used for
	 * @param description the description of this step
	 * @param the name under which the changes this page should apply are stored in the WizardSettings
	 */
	public ChangeApplication(String wizardStepNo, String description, String keyChangesToApply) {
		
		super(wizardStepNo, description);
		
		this.keyChangesToApply = keyChangesToApply;
		
		JLabel lblParsingInputfile = new JLabel("Applying approved changes from last step...");
		getPb_applyChanges().setStringPainted(true);
		textArea = new AutoscrollTextPane();
		getTextArea().setEditable(false);
		
		JScrollPane text_sp = new JScrollPane(getTextArea());
		getTextArea().setScrollPane(text_sp);
		text_sp.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		
		JLabel lblLog = new JLabel("Status:");

		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(Alignment.TRAILING, groupLayout.createSequentialGroup()
							.addContainerGap()
							.addComponent(text_sp, GroupLayout.DEFAULT_SIZE, 430, Short.MAX_VALUE))
						.addGroup(groupLayout.createSequentialGroup()
							.addContainerGap()
							.addComponent(lblLog))
						.addGroup(groupLayout.createSequentialGroup()
							.addContainerGap()
							.addComponent(pb_applyChanges, GroupLayout.DEFAULT_SIZE, 430, Short.MAX_VALUE))
						.addGroup(groupLayout.createSequentialGroup()
							.addContainerGap()
							.addComponent(lblParsingInputfile)))
					.addContainerGap())
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblParsingInputfile)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(pb_applyChanges, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblLog)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(text_sp, GroupLayout.DEFAULT_SIZE, 210, Short.MAX_VALUE)
					.addContainerGap())
		);
		setLayout(groupLayout);
	}
	
	/**
	 * Method called during the rendering of this WizardPage.
	 * Used for necessary initializations on the page and to start the process
	 */
	public void rendering(List<WizardPage> path, WizardSettings settings) {
		
		setNextEnabled(false);
		setPrevEnabled(false);

		@SuppressWarnings("unchecked")
		Map<Integer, ApprovedChangeRec> changesToApply = (Map<Integer, ApprovedChangeRec>) settings.get(keyChangesToApply);
		@SuppressWarnings("unchecked")
		Map<Integer, RenewalData> renewalData = (Map<Integer, RenewalData>) settings.get("renewalDataIndexed");
		locSettings = settings;
		ca = new ChangeApplier(textArea, renewalData, changesToApply);
		ca.addPropertyChangeListener(this);
		ca.execute();
		
	}
	
	/**
	 * Method that is called on propertyChange when this class is registered as listener.
	 * @param evt the PropertyChangeEvent
	 */
	public void propertyChange(PropertyChangeEvent evt) {

		if ("progress".equals(evt.getPropertyName())) {

			if (evt.getSource() instanceof ChangeApplier) {
				pb_applyChanges.setValue((Integer) evt.getNewValue());
			}
			
		} else if ("state".equals(evt.getPropertyName())) {
			
			if (evt.getSource() instanceof ChangeApplier) {
				if (evt.getNewValue() == SwingWorker.StateValue.STARTED) {
					textArea.appendNormalText("Started application of approved changes from last step:\n");
				} else if (evt.getNewValue() == SwingWorker.StateValue.DONE) {
					pb_applyChanges.setValue(100);
					textArea.appendNormalText("Application of changes finished. You can continue with the next step.");
					locSettings.put("renewalDataIndexed", ca.getRenewalData());
					setNextEnabled(true);
				}
			}
		}
		
	}
	
	/**
	 * @return the textArea
	 */
	public AutoscrollTextPane getTextArea() {
		return textArea;
	}

	/**
	 * @return the pb_parseInput
	 */
	public JProgressBar getPb_applyChanges() {
		return pb_applyChanges;
	}

}
