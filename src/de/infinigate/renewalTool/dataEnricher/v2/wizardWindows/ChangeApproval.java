package de.infinigate.renewalTool.dataEnricher.v2.wizardWindows;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import org.ciscavate.cjwizard.WizardPage;
import org.ciscavate.cjwizard.WizardSettings;

import de.infinigate.renewalTool.dataEnricher.v2.data.ApprovedChangeRec;
import de.infinigate.renewalTool.dataEnricher.v2.data.RenewalData;
import de.infinigate.renewalTool.dataEnricher.v2.gui.ChangeApprovalTable;

/**
 * WizardPage that displays suggested changes to the user for him to approve them.
 * @author mathias.mueller
 */
public class ChangeApproval extends WizardPage {

	/**
	 * Generated serialVersionUID. 
	 */
	private static final long serialVersionUID = 8653838463593636939L;

	/**
	 * Flag indicating, whether this WizardPage was already rendered.
	 */
	private boolean rendered;

	/**
	 * The data for which changes were suggested.
	 */
	private Map<Integer, RenewalData> renewalData;
	
	/**
	 * The suggested changes.
	 */
	private Map<Integer, List<Map<String, String>>> suggestedChanges;

	/**
	 * The name of the changes this specific instance should process (class is reusable).
	 */
	private String nameOfChanges;
	
	/**
	 * Reference to the table on this WizardPage.
	 */
	private static ChangeApprovalTable table;
	
	/**
	 * Create the WizardPage.
	 */
	public ChangeApproval(String wizardStepNo, String description, String nameOfChanges) {
		super(wizardStepNo, description);
		setLayout(new GridBagLayout());
		this.nameOfChanges = nameOfChanges;
		rendered = false;
	}
	
	/**
	 * Method called during the rendering of this WizardPage.
	 * Used for necessary initializations on the page
	 */
	public void rendering(List<WizardPage> path, WizardSettings settings) {
		super.rendering(path, settings);
		setPrevEnabled(false);
		
		suggestedChanges = (Map<Integer, List<Map<String, String>>>) settings.get(nameOfChanges);
		renewalData = (Map<Integer, RenewalData>) settings.get("renewalDataIndexed");
		if (!rendered) {
			rendered = true;
			if (suggestedChanges != null && renewalData != null) { 

				GridBagConstraints c_table = new GridBagConstraints();
				c_table.gridx = 0;
				c_table.gridy = 0;
				c_table.weightx = 1;
				c_table.weighty = 1;
				c_table.fill = GridBagConstraints.BOTH;
				
				table = new ChangeApprovalTable(suggestedChanges, renewalData);
				table.setPreferredScrollableViewportSize(new Dimension(500,100));
				table.setFillsViewportHeight(true);
				
				JScrollPane table_sp = new JScrollPane(table);
				add(table_sp, c_table);
				
				JPanel btn_pane = new JPanel();
				
				JButton btn_selectAll = new JButton("Select all");		
				ActionListener al_all = new ActionListener() {

					public void actionPerformed(ActionEvent arg0) {
						Map<Integer, ApprovedChangeRec> fullMap = table.getApprovedChanges();
						for (ApprovedChangeRec currRec : fullMap.values()) {
							currRec.setApplyChange(true);
						}
						table.updateUI();
					}
				};
				
				btn_selectAll.addActionListener(al_all);
				
				JButton btn_selectNone = new JButton("Select none");		
				ActionListener al_none = new ActionListener() {

					public void actionPerformed(ActionEvent arg0) {
						Map<Integer, ApprovedChangeRec> fullMap = table.getApprovedChanges();
						for (ApprovedChangeRec currRec : fullMap.values()) {
							currRec.setApplyChange(false);
						}
						table.updateUI();
						
					}
					
				};
				btn_selectNone.addActionListener(al_none);
				
				btn_pane.setLayout(new GridLayout(0,2));
				btn_pane.add(btn_selectAll);
				btn_pane.add(btn_selectNone);
				GridBagConstraints c_pane = new GridBagConstraints();
				c_pane.gridx = 0;
				c_pane.gridy = 1;
				c_pane.weightx = 0;
				c_pane.weighty = 0;
				c_pane.anchor = GridBagConstraints.WEST;
				add(btn_pane, c_pane);
				updateUI();
			} 
		}
	}

	/**
	 * @return the nameOfChanges
	 */
	public String getNameOfChanges() {
		return nameOfChanges;
	}
	
	/**
	 * Returns the approvedChanges after the table was displayed once.. 
	 * @return the approved changes
	 */
	public Map<Integer, ApprovedChangeRec> getApprovedChanges() {
		return table.getApprovedChanges();
	}
	
}
