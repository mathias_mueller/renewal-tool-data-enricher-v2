package de.infinigate.renewalTool.dataEnricher.v2.wizardWindows;

import java.awt.Font;
import java.util.List;

import org.ciscavate.cjwizard.WizardPage;
import org.ciscavate.cjwizard.WizardSettings;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.GroupLayout.Alignment;

/**
 * Last page in the wizard.
 * @author Mathias.mueller
 */
public class Done extends WizardPage {

	/**
	 * Generated serialVersionUID.
	 */
	private static final long serialVersionUID = -1988932977247426325L;

	/** 
	 * Build this WizardPage.
	 * @param wizardStepNo
	 */
	public Done(String wizardStepNo) {
		
		super(wizardStepNo, "Process complete.");
		JLabel lblWelcomeToThe = new JLabel("The process of enriching your RenewalData has been completed!");
		lblWelcomeToThe.setFont(new Font("Tahoma", Font.PLAIN, 13));
		
		JLabel lblThisProgramWill = new JLabel("<html>All changes have been re-uploaded to the database and can now be checked in the Renewal Worksheet in Navision.</html>");
		lblThisProgramWill.setFont(new Font("Tahoma", Font.PLAIN, 12));
		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(Alignment.TRAILING, groupLayout.createSequentialGroup()
					.addGap(20)
					.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
						.addComponent(lblWelcomeToThe, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 420, Short.MAX_VALUE)
						.addComponent(lblThisProgramWill, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 420, Short.MAX_VALUE))
					.addContainerGap())
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(34)
					.addComponent(lblWelcomeToThe)
					.addGap(18)
					.addComponent(lblThisProgramWill, GroupLayout.PREFERRED_SIZE, 58, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(174, Short.MAX_VALUE))
		);
		setLayout(groupLayout);
		
	}
	

	/**
	 * Method called during the rendering of this WizardPage.
	 * Used for necessary initializations on the page
	 */
	public void rendering(List<WizardPage> path, WizardSettings settings) {
	
		setNextEnabled(false);
		setPrevEnabled(false);
		setFinishEnabled(true);
			
	}
	
}
