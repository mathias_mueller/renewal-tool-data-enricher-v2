package de.infinigate.renewalTool.dataEnricher.v2.wizardWindows;

import java.awt.Font;

import org.ciscavate.cjwizard.WizardPage;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import javax.swing.JCheckBox;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingConstants;

public class OptionSelection extends WizardPage {

	/**
	 * Auto-generated serialVersionUID.
	 */
	private static final long serialVersionUID = 1402812526005923035L;

	/**
	 * Create the panel.
	 */
	public OptionSelection() {

		super("3","");
		
		JLabel lblpleaseSelectOne = new JLabel("<html>Please select one or more of the available enrichment processes:</html>");
		lblpleaseSelectOne.setFont(new Font("Tahoma", Font.PLAIN, 13));
		
		JCheckBox cb_ResellerNameEnrichment = new JCheckBox("Obtain \"Reseller Name\" from \"Serial No.\"s");
		cb_ResellerNameEnrichment.setFont(new Font("Tahoma", Font.PLAIN, 13));
		cb_ResellerNameEnrichment.setName("resellerNameEnrichment");
		
		JLabel lblAfterSelectingThe = new JLabel("<html>After selecting the desired options, click \"Next >\" to continue with enriching your data.</html>");
		lblAfterSelectingThe.setFont(new Font("Tahoma", Font.PLAIN, 13));
		
		JCheckBox cb_emailCheck = new JCheckBox("<html>Check data for records with same \"Reseller Name\" but different \"E-Mail TO\"</html>");
		cb_emailCheck.setVerticalAlignment(SwingConstants.TOP);
		cb_emailCheck.setName("ambiguousEmailCheck");
		cb_emailCheck.setFont(new Font("Tahoma", Font.PLAIN, 13));
		
		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(cb_emailCheck, GroupLayout.PREFERRED_SIZE, 408, GroupLayout.PREFERRED_SIZE)
							.addContainerGap())
						.addGroup(groupLayout.createSequentialGroup()
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addGroup(groupLayout.createSequentialGroup()
									.addComponent(cb_ResellerNameEnrichment, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
									.addGap(155))
								.addComponent(lblpleaseSelectOne, GroupLayout.DEFAULT_SIZE, 422, Short.MAX_VALUE))
							.addGap(18))
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(lblAfterSelectingThe, GroupLayout.DEFAULT_SIZE, 422, Short.MAX_VALUE)
							.addGap(18))))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblpleaseSelectOne)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(cb_ResellerNameEnrichment)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(cb_emailCheck, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(lblAfterSelectingThe, GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(160, Short.MAX_VALUE))
		);
		setLayout(groupLayout);
			
	}
}
