package de.infinigate.renewalTool.dataEnricher.v2.wizardWindows;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.ciscavate.cjwizard.WizardPage;
import org.ciscavate.cjwizard.WizardSettings;

import de.infinigate.renewalTool.dataEnricher.v2.common.XmlConfigManager;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.LayoutStyle.ComponentPlacement;

import java.awt.Font;

/**
 * Wizard Page No. 2.
 * @author mathias.mueller
 */
public class RenewalProfileSelection extends WizardPage {

	public static final String RENEWAL_PROFILE_SETTINGS_KEY = "renewalProfile";

	/**
	 * Generated serialVersionUID.
	 */
	private static final long serialVersionUID = 24879573480994894L;

	/**
	 * Flag that shows if the ComboBox is already filled.
	 */
	private boolean cBoxFilled = false;
	
	private JComboBox<String> cbRenewalProfile;
	
	/**
	 * Create the panel.
	 */
	public RenewalProfileSelection() {
		
		super("2","");
		
		cbRenewalProfile = new JComboBox<String>();
		cbRenewalProfile.setFont(new Font("Tahoma", Font.PLAIN, 12));
		cbRenewalProfile.setName(RENEWAL_PROFILE_SETTINGS_KEY);
		
		JLabel lblNewLabel = new JLabel("Please select the Renewal-Profile, whose data you want to improve.");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 12));
		
		JLabel lblNewLabel_1 = new JLabel("<html>Click 'Next >', to determine which of the available enrichment processes shall be used.<br/><br/>WARNING: This program will modify data without further prompt!</html>");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 12));
		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(20)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(cbRenewalProfile, GroupLayout.PREFERRED_SIZE, 215, GroupLayout.PREFERRED_SIZE)
							.addGap(40))
						.addGroup(Alignment.TRAILING, groupLayout.createSequentialGroup()
							.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
								.addComponent(lblNewLabel, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 420, Short.MAX_VALUE)
								.addComponent(lblNewLabel_1, GroupLayout.DEFAULT_SIZE, 420, Short.MAX_VALUE))
							.addContainerGap())))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(41)
					.addComponent(cbRenewalProfile, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addGap(11)
					.addComponent(lblNewLabel)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(lblNewLabel_1, GroupLayout.PREFERRED_SIZE, 67, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(134, Short.MAX_VALUE))
		);
		setLayout(groupLayout);

	}
	
	/**
	 * Method called during the rendering of this WizardPage.
	 * Used for necessary initializations on the page
	 */
	public void rendering(List<WizardPage> path, WizardSettings settings) {
		super.rendering(path, settings);
		
		if (!cBoxFilled) {
			try {
				Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			} catch (ClassNotFoundException e) {
				JOptionPane.showMessageDialog(null, "Error loading SQL-Driver!", "Error", JOptionPane.ERROR_MESSAGE);
				System.exit(1);
			}
			XmlConfigManager xmlConfig = new XmlConfigManager();
			settings.put("xmlConfig", xmlConfig);
			
			String connectionUrl = "jdbc:sqlserver://" + xmlConfig.getSqlAddress() + ";integratedSecurity=true";
			Connection con = null;
			try {
				con = DriverManager.getConnection(connectionUrl);
			} catch (SQLException e) {
				JOptionPane.showMessageDialog(null, "Error connecting to SQL-DB! Message:\n" + e.getMessage(), 
						"Error", JOptionPane.ERROR_MESSAGE);
				System.exit(1);
			}
			
			ResultSet results = null;
			try {
				Statement stmt = con.createStatement();
				results = stmt.executeQuery("SELECT [Profile Code] FROM [" + xmlConfig.getNavDbName() + "].dbo.[" + 
						xmlConfig.getNavCompanyName() + "$_Renewal Import Profile]");
			} catch (SQLException e) {
				JOptionPane.showMessageDialog(null, "Error creating SQL-Statement! Message:\n" + e.getMessage(), 
						"Error", JOptionPane.ERROR_MESSAGE);
				System.exit(1);
			}
			
			try {
				while (results.next()) {
					cbRenewalProfile.addItem(results.getString(1));
				}
				cBoxFilled = true;
			} catch (SQLException e) {
				JOptionPane.showMessageDialog(null, "Error reading results from DB! Message:\n" + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
				System.exit(1);
			}
		}
		
	}
}
