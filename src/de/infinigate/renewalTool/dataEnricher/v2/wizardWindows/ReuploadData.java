package de.infinigate.renewalTool.dataEnricher.v2.wizardWindows;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.List;
import java.util.Map;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.SwingWorker;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;

import org.ciscavate.cjwizard.WizardPage;
import org.ciscavate.cjwizard.WizardSettings;

import de.infinigate.renewalTool.dataEnricher.v2.common.XmlConfigManager;
import de.infinigate.renewalTool.dataEnricher.v2.data.RenewalData;
import de.infinigate.renewalTool.dataEnricher.v2.gui.AutoscrollTextPane;
import de.infinigate.renewalTool.dataEnricher.v2.workers.SQLDataUploader;

public class ReuploadData extends WizardPage implements PropertyChangeListener {

	/**
	 * Generated serialVersionUID.
	 */
	private static final long serialVersionUID = 3237534075917733678L;

	/**
	 * The progressBar for change application.
	 */
	private final JProgressBar pb_uploadData = new JProgressBar();
	
	/**
	 * This window's textArea for status messages.
	 */
	private final AutoscrollTextPane textArea;

	/**
	 * Worker-Class used to reupload the data to the database.
	 */
	private SQLDataUploader dataUpl;
	
	/**
	 * Create the panel.
	 */
	public ReuploadData(String wizardStepNo) {
		
		super(wizardStepNo, "Reuploading modified data.");
						
		JLabel lblReuploadingData = new JLabel("Reuploading data to DB...");
		getPb_uploadData().setStringPainted(true);
		textArea = new AutoscrollTextPane();
		getTextArea().setEditable(false);
		
		JScrollPane text_sp = new JScrollPane(getTextArea());
		getTextArea().setScrollPane(text_sp);
		text_sp.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		
		JLabel lblLog = new JLabel("Status:");

		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(Alignment.TRAILING, groupLayout.createSequentialGroup()
							.addContainerGap()
							.addComponent(text_sp, GroupLayout.DEFAULT_SIZE, 430, Short.MAX_VALUE))
						.addGroup(groupLayout.createSequentialGroup()
							.addContainerGap()
							.addComponent(lblLog))
						.addGroup(groupLayout.createSequentialGroup()
							.addContainerGap()
							.addComponent(pb_uploadData, GroupLayout.DEFAULT_SIZE, 430, Short.MAX_VALUE))
						.addGroup(groupLayout.createSequentialGroup()
							.addContainerGap()
							.addComponent(lblReuploadingData)))
					.addContainerGap())
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblReuploadingData)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(pb_uploadData, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblLog)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(text_sp, GroupLayout.DEFAULT_SIZE, 210, Short.MAX_VALUE)
					.addContainerGap())
		);
		setLayout(groupLayout);

	}
	
	/**
	 * Method called during the rendering of this WizardPage.
	 * Used for necessary initializations on the page and to start the process
	 */
	public void rendering(List<WizardPage> path, WizardSettings settings) {
		
		setNextEnabled(false);
		setPrevEnabled(false);
		
		XmlConfigManager xmlConfig = (XmlConfigManager) settings.get("xmlConfig");
		Map<Integer, RenewalData> renewalDataIndexed = (Map<Integer, RenewalData>) settings.get("renewalDataIndexed");
		
		dataUpl = new SQLDataUploader(xmlConfig);
		dataUpl.addPropertyChangeListener(this);
		dataUpl.setDataToUpload(renewalDataIndexed);
		dataUpl.execute();
		
	}
	
	/**
	 * Called on property change.
	 * @param evt the PropertyChangeEvent to handle
	 */
	public void propertyChange(PropertyChangeEvent evt) {

		if ("progress".equals(evt.getPropertyName())) {
			if (evt.getSource() instanceof SQLDataUploader) {
				getPb_uploadData().setValue((Integer) evt.getNewValue());
			}
		} else if ("state".equals(evt.getPropertyName())) { 
			if (evt.getSource() instanceof SQLDataUploader) {
				if (evt.getNewValue() == SwingWorker.StateValue.STARTED) {
					textArea.appendNormalText("Starting to reupload modified data to the database...\n");
				} else if (evt.getNewValue() == SwingWorker.StateValue.DONE) {
					getPb_uploadData().setValue(100);
					textArea.appendNormalText("All modified data was successfully uploaded.\n");
					textArea.appendNormalText(dataUpl.getRecsProcessed() + " records were processed, " + dataUpl.getRecsUpdated() 
							+ " of them was/were updated.");
					setNextEnabled(true);
				}
			}
		}
	}

	/**
	 * @return the pb_applyChanges
	 */
	public JProgressBar getPb_uploadData() {
		return pb_uploadData;
	}

	/**
	 * @return the textArea
	 */
	public AutoscrollTextPane getTextArea() {
		return textArea;
	}

}
