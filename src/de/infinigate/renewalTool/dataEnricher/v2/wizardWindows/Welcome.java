package de.infinigate.renewalTool.dataEnricher.v2.wizardWindows;

import java.awt.Font;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.GroupLayout.Alignment;

import org.ciscavate.cjwizard.WizardPage;

public class Welcome extends WizardPage {

	/**
	 * Auto generated serialVersionUID.
	 */
	private static final long serialVersionUID = -553885482727153832L;

	/**
	 * Create the panel.
	 */
	public Welcome() {
		
		super("1","");

		JLabel lblWelcomeToThe = new JLabel("Welcome to the Infinigate Renewal Data Enricher!");
		lblWelcomeToThe.setFont(new Font("Tahoma", Font.PLAIN, 13));
		
		JLabel lblThisProgramWill = new JLabel("<html>This program will guide you through the process of improving <br/>your Renewal-data using the data present in Microsoft Dynamics NAV.</html>");
		lblThisProgramWill.setFont(new Font("Tahoma", Font.PLAIN, 12));
		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(Alignment.TRAILING, groupLayout.createSequentialGroup()
					.addGap(20)
					.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
						.addComponent(lblWelcomeToThe, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 420, Short.MAX_VALUE)
						.addComponent(lblThisProgramWill, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 420, Short.MAX_VALUE))
					.addContainerGap())
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(34)
					.addComponent(lblWelcomeToThe)
					.addGap(18)
					.addComponent(lblThisProgramWill, GroupLayout.PREFERRED_SIZE, 58, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(174, Short.MAX_VALUE))
		);
		setLayout(groupLayout);
	}
}
