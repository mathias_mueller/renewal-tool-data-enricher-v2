package de.infinigate.renewalTool.dataEnricher.v2.wizardWindows;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.List;
import java.util.Map;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingWorker;

import org.ciscavate.cjwizard.WizardPage;
import org.ciscavate.cjwizard.WizardSettings;

import de.infinigate.renewalTool.dataEnricher.v2.common.XmlConfigManager;
import de.infinigate.renewalTool.dataEnricher.v2.data.RenewalData;
import de.infinigate.renewalTool.dataEnricher.v2.gui.AutoscrollTextPane;
import de.infinigate.renewalTool.dataEnricher.v2.workers.AbstractDataEnricherWorker;
import de.infinigate.renewalTool.dataEnricher.v2.workers.RenewalDataObtainer;

public class Working extends WizardPage implements PropertyChangeListener {

	/**
	 * Auto-generated serialVersionUID.
	 */
	private static final long serialVersionUID = 2004310200720308653L;

	/**
	 * The progressBar for input file parsing.
	 */
	private final JProgressBar pb_working = new JProgressBar();
	
	/**
	 * This window's textArea for status messages.
	 */
	private final AutoscrollTextPane textArea;
	
	/**
	 * The worker doing the enrichment in this step.
	 */
	private final AbstractDataEnricherWorker<Void, Void> worker;

	/**
	 * Reference to a RenewalDataObtainer in case the data still needs to be obtained from DB.
	 */
	private RenewalDataObtainer rdo;

	/**
	 * Local reference to this program's XmlConfigManager.
	 */
	private XmlConfigManager xmlConfig;

	/**
	 * Output received from the AbstractDataEnricherWorker that works in this object.
	 */
	private Map<Integer, List<Map<String, String>>> output;
	
	/**
	 * Local reference to the WizardSettings object used.
	 */
	private WizardSettings locSettings;

	/**
	 * Flag indicating, whether the current operation yielded any results.
	 */
	private boolean suggestedChangesFound = false;
	
	/**
	 * Create the page.
	 * @param wizardStepNo the step of the wizard that this object is to fulfill
	 * @param description the description that should be used (diplayed as first line on page)
	 * @param worker the SwingWorker that does the work in this step
	 */
	public Working(String wizardStepNo, String description, AbstractDataEnricherWorker<Void, Void> worker) {

		super(wizardStepNo, description);
		
		this.worker = worker;
		worker.addPropertyChangeListener(this);
		
		JLabel lblWorking = new JLabel("Working...");
		getPb_working().setStringPainted(true);
		textArea = new AutoscrollTextPane();
		getTextArea().setEditable(false);
		
		JScrollPane text_sp = new JScrollPane(getTextArea());
		getTextArea().setScrollPane(text_sp);
		text_sp.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		
		JLabel lblLog = new JLabel("Status:");

		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(Alignment.TRAILING, groupLayout.createSequentialGroup()
							.addContainerGap()
							.addComponent(text_sp, GroupLayout.DEFAULT_SIZE, 430, Short.MAX_VALUE))
						.addGroup(groupLayout.createSequentialGroup()
							.addContainerGap()
							.addComponent(lblLog))
						.addGroup(groupLayout.createSequentialGroup()
							.addContainerGap()
							.addComponent(pb_working, GroupLayout.DEFAULT_SIZE, 430, Short.MAX_VALUE))
						.addGroup(groupLayout.createSequentialGroup()
							.addContainerGap()
							.addComponent(lblWorking)))
					.addContainerGap())
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblWorking)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(pb_working, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblLog)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(text_sp, GroupLayout.DEFAULT_SIZE, 210, Short.MAX_VALUE)
					.addContainerGap())
		);
		setLayout(groupLayout);
	}
	
	/**
	 * Method called during the rendering of this WizardPage.
	 * Used for necessary initializations on the page
	 */
	public void rendering(List<WizardPage> path, WizardSettings settings) {
		
		super.rendering(path, settings);
		setNextEnabled(false);
		setPrevEnabled(false);
		
		locSettings = settings;
		
		RenewalData[] data = (RenewalData[]) settings.get("renewalData");
		xmlConfig = (XmlConfigManager) settings.get("xmlConfig");
		
		if (data == null) {
			// obtain data from DB, none present in settings.
			textArea.appendNormalText("First enrichment process, data needs to be obtained from DB...");
			String importProfileCode = (String) settings.get("renewalProfile");
			
			if (importProfileCode == null || xmlConfig == null) { 
				JOptionPane.showMessageDialog(null,"Error obtaining RenewalData", "\n ERROR: Couldn't obtain importProfileCode and/or xmlConfig when trying to read data.", JOptionPane.ERROR_MESSAGE);
			} else {
				rdo = new RenewalDataObtainer(xmlConfig, importProfileCode);
				rdo.addPropertyChangeListener(this);
				rdo.execute();
			}
		} else {
			worker.setInputData(data);
			worker.setXmlConfig(xmlConfig);
			worker.execute();
		}
		
	}

	public void propertyChange(PropertyChangeEvent evt) {

		if ("progress".equals(evt.getPropertyName())) {
			if (evt.getSource() instanceof AbstractDataEnricherWorker) {
				pb_working.setValue((Integer) evt.getNewValue());
			}
		} else if ("state".equals(evt.getPropertyName())) {
			if (evt.getSource() instanceof RenewalDataObtainer) {
				if (evt.getNewValue() == SwingWorker.StateValue.DONE) {
					textArea.appendNormalText("done.\n");
					locSettings.put("renewalDataIndexed", rdo.getOutputIndexed());
					worker.setInputData(rdo.getOutput());
					worker.setXmlConfig(xmlConfig);
					worker.execute();
				}
			} else if (evt.getSource() instanceof AbstractDataEnricherWorker) {
				if (evt.getNewValue() == SwingWorker.StateValue.STARTED) {
					textArea.appendNormalText("____________________________________" + "\n" + worker.getDescription() + "...\n");
				} else if (evt.getNewValue() == SwingWorker.StateValue.DONE) {
					textArea.appendNormalText("'" + worker.getDescription() + "' completed.");
					output = worker.getOutput();
					setSuggestedChangesFound(output.size() > 0);
					if (getSuggestedChangesFound()){
						locSettings.put(worker.getSettingsSaveName(), output);
						textArea.appendNormalText("\n" + output.size() + " suggested changes could be found, opening the change approval form next!");
					} else {
						textArea.appendNormalText("\nThere were no suggestions for data enrichments found in this step. Continuing to the next/the end.");
					}
					setNextEnabled(true);
				}
			}
		}
		
	}
	
	/**
	 * @return the pb_parseInput
	 */
	public JProgressBar getPb_working() {
		return pb_working;
	}

	/**
	 * @return the textArea
	 */
	public AutoscrollTextPane getTextArea() {
		return textArea;
	}

	/**
	 * @return the output
	 */
	public Map<Integer, List<Map<String, String>>> getOutput() {
		return output;
	}

	/**
	 * @return the suggestedChangesFound
	 */
	public boolean getSuggestedChangesFound() {
		return suggestedChangesFound;
	}

	/**
	 * @param suggestedChangesFound the suggestedChangesFound to set
	 */
	public void setSuggestedChangesFound(boolean suggestedChangesFound) {
		this.suggestedChangesFound = suggestedChangesFound;
	}
	
}
