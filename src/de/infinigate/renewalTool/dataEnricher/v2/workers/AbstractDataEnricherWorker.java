package de.infinigate.renewalTool.dataEnricher.v2.workers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.swing.SwingWorker;

import de.infinigate.renewalTool.dataEnricher.v2.common.XmlConfigManager;
import de.infinigate.renewalTool.dataEnricher.v2.data.RenewalData;
import de.infinigate.renewalTool.dataEnricher.v2.gui.AutoscrollTextPane;

public abstract class AbstractDataEnricherWorker<T, V> extends SwingWorker<T, V> {

	/**
	 * The data at the beginning of this worker's work.
	 */
	private RenewalData[] inputData;

	/**
	 * A map containing the suggested enrichments for a RenewalData record (identified by the Integer ment for an "ID"-val).
	 * Maps IDs (-> Integer) to an Array of Maps<String,String> of suggested modifications where <"fieldname","fieldvalue"> and each
	 *   array element is a possible match
	 */
	protected Map<Integer, List<Map<String,String>>> output = new HashMap<Integer, List<Map<String, String>>>();
	
	/**
	 * String that tells what kind of enrichment this worker does.
	 */
	private final String description;
	
	/**
	 * The name under which the output from this worker must be saved in order for the next step to find it.
	 * MUST override this
	 */
	private static final String SETTINGS_SAVE_NAME = null;
	
	/**
	 * Local reference to this program's XmlConfigManager object.
	 */
	private XmlConfigManager xmlConfig = null;
	
	/**
	 * TextArea that can be used for direct output of status information.
	 */
	private AutoscrollTextPane textArea = null;
	
	/**
	 * Basic CTor for implementations of this abstract class.
	 * @param description a description of what this DataEnricherWorker does
	 */
	public AbstractDataEnricherWorker(String description) {
		this.description = description;
	}
	
	/**
	 * @param inputData the inputData to set
	 */
	public void setInputData(RenewalData[] inputData) {
		this.inputData = inputData;
	}
	
	/**
	 * Getter for the inputData.
	 * @return the inputData
	 */
	public RenewalData[] getInputData() {
		return inputData;
	}

	/**
	 * @return the output
	 */
	public Map<Integer, List<Map<String, String>>> getOutput() {
		return output;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @return the xmlConfig
	 */
	public XmlConfigManager getXmlConfig() {
		return xmlConfig;
	}

	/**
	 * @param xmlConfig the xmlConfig to set
	 */
	public void setXmlConfig(XmlConfigManager xmlConfig) {
		this.xmlConfig = xmlConfig;
	}

	/**
	 * @return the textArea
	 */
	public AutoscrollTextPane getTextArea() {
		return textArea;
	}

	/**
	 * @param textArea the textArea to set
	 */
	public void setTextArea(AutoscrollTextPane textArea) {
		this.textArea = textArea;
	}
	
	/**
	 * Helper method to append Text to this object's textArea, if ref is set.
	 * Does nothing if textArea hasn't been set before calling
	 * @param textToAppend the text that should be appended
	 * @param textType tells whether the text should be displayed as normal text (0), warning message (1) or critical warning message (2)
	 */
	protected void appendTextToTextArea(String textToAppend, int textType) {
		if (this.textArea != null) {
			switch (textType) {
			case 0:
				this.textArea.appendNormalText(textToAppend);
				break;
			case 1:
				this.textArea.appendErrorText(textToAppend);
				break;
			case 2:
				this.textArea.appendCriticalErrorText(textToAppend);
				break;
			}
		}
	}

	/**
	 * Checks if one of the values from a Map<String,String> is different from the one entered for a RenewalData-record.
	 * @param currRecToCheck the RenewalData rec to check
	 * @param valsToCheckFor the Array containing the values to check for
	 * @return whether there is a field different from the RenewalData-field in the map given as param 
	 */
	protected boolean oneValueDifferentFromCurrentInRec(
			RenewalData currRecToCheck,
			ArrayList<Map<String, String>> valsToCheckFor) {
		
		for (Map<String,String> currMap : valsToCheckFor) {
			for (Entry<String,String> currEntry : currMap.entrySet()) {
				if (!currEntry.getValue().equals(currRecToCheck.getFieldValByName(currEntry.getKey())))
					return true;		
			}
		}
		
		return false;
	}

	/**
	 * @return the settingsSaveName
	 */
	public abstract String getSettingsSaveName();
		
}
