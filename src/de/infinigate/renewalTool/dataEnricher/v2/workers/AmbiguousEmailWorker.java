package de.infinigate.renewalTool.dataEnricher.v2.workers;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.swing.JOptionPane;
import javax.swing.SwingWorker;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;

import de.infinigate.renewalTool.dataEnricher.v2.data.AmbiguousEmailData;
import de.infinigate.renewalTool.dataEnricher.v2.data.RenewalData;

public class AmbiguousEmailWorker extends SwingWorker<Void, Void> {
	
	Map<Integer, RenewalData> renewalDataIndexed;
	
	private boolean dataWithAmbiguousEmailsPresent = false;;
	
	private AmbiguousEmailData[] ambiguousEmailData;
	
	public AmbiguousEmailWorker(Map<Integer, RenewalData> renewalDataIndexed) {
		this.renewalDataIndexed = renewalDataIndexed;
	}

	@Override
	protected Void doInBackground() throws Exception {

		if (renewalDataIndexed == null) 
			JOptionPane.showMessageDialog(null, "The AmbiguousEmailWorker was unable to find any RenewalData, something "
					+ "must have gone wrong in the process.", "No RenewalData found" , JOptionPane.ERROR_MESSAGE);

		Multimap<String, RenewalData> renewalDataByResellerName = HashMultimap.create();
		
		for (RenewalData currRenewalDataRec : renewalDataIndexed.values()) {
			renewalDataByResellerName.put(currRenewalDataRec.getResellerName(), currRenewalDataRec);
		}
		
		setProgress(33);
		
		List<AmbiguousEmailData> ambiguousEmails = new ArrayList<AmbiguousEmailData>();
		Set<String> keysWithMultipleMails = new HashSet<String>();
		for (String currKey : renewalDataByResellerName.keySet()) {
			
			String lastEmailRead = null; 
			for (RenewalData currRec : renewalDataByResellerName.get(currKey)) {
			
				if (lastEmailRead == null)
					lastEmailRead = currRec.geteMailTo();
				else if (!lastEmailRead.equals(currRec.geteMailTo())) {
					keysWithMultipleMails.add(currKey);
				}
			}
		}
		
		setProgress(66);
		
		if (keysWithMultipleMails.size() > 0) {
			for (String currKey : keysWithMultipleMails) {
				List<String> allAddressesForKey = new ArrayList<String>(); 
				for (RenewalData currRec : renewalDataByResellerName.get(currKey)) {
					allAddressesForKey.add(currRec.geteMailTo());
				}
				ambiguousEmails.add(new AmbiguousEmailData(true, currKey, allAddressesForKey.toArray(new String[5]), 
						currKey, allAddressesForKey.get(0)));
			}
		}
		
		ambiguousEmailData = ambiguousEmails.toArray(new AmbiguousEmailData[0]);
		if (ambiguousEmails.size() > 0) 
			dataWithAmbiguousEmailsPresent = true;
		
		setProgress(100);
		
		return null;
	}

	/**
	 * @return the dataWithAmbiguousEmailsPresent
	 */
	public boolean isDataWithAmbiguousEmailsPresent() {
		return dataWithAmbiguousEmailsPresent;
	}

	/**
	 * @return the ambiguousEmailData
	 */
	public AmbiguousEmailData[] getAmbiguousEmailData() {
		return ambiguousEmailData;
	}

}
