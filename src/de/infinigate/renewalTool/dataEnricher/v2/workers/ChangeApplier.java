package de.infinigate.renewalTool.dataEnricher.v2.workers;

import java.util.Map;
import java.util.Map.Entry;

import javax.swing.SwingWorker;

import de.infinigate.renewalTool.dataEnricher.v2.data.ApprovedChangeRec;
import de.infinigate.renewalTool.dataEnricher.v2.data.RenewalData;
import de.infinigate.renewalTool.dataEnricher.v2.gui.AutoscrollTextPane;

/**
 * Class that applies changes given to the renewalData (also given).
 * @author mathias.mueller
 */
public class ChangeApplier extends SwingWorker<Void, Void> {
	
	/**
	 * The RenewalData to apply changes to.
	 */
	private Map<Integer, RenewalData> renewalData;
	
	/**
	 * The changes to apply.
	 */
	private Map<Integer, ApprovedChangeRec> changesToApply;
	
	/**
	 * TextArea of parent window to display information.
	 */
	private AutoscrollTextPane statusTextArea;
	
	/**
	 * Create a ChangeApplier.
	 * @param statusTextArea textArea of parent window to be able to give status and general information
	 * @param renewalData the data to apply changes to
	 * @param changesToApply the changes to apply
	 */
	public ChangeApplier(AutoscrollTextPane statusTextArea, Map<Integer, RenewalData> renewalData,
			Map<Integer, ApprovedChangeRec> changesToApply) {
		this.statusTextArea = statusTextArea;
		this.renewalData = renewalData;
		this.changesToApply = changesToApply;
	}

	/**
	 * The method that does the actual work of this Class.
	 */
	@Override
	protected Void doInBackground() throws Exception {
		boolean dataMissing = false;
		
		if (renewalData == null) {
			statusTextArea.appendErrorText("\nNo RenewalData was found, aborting.");
			dataMissing = true;
		}
		if (changesToApply == null) {
			statusTextArea.appendErrorText("\nNo changes to apply were found, aborting.");
			dataMissing = true;
		}
		if (!dataMissing) {
			int totalChangesToApplyCount = changesToApply.size();
			int changesApplied = 0;
			for (Entry<Integer, ApprovedChangeRec> currEntry : changesToApply.entrySet()) {
				if (currEntry.getValue().isApplyChange()) {
					RenewalData renewalDataRecToModify = renewalData.get(currEntry.getKey());
					for (Entry<String,String> currChange : currEntry.getValue().getChanges().entrySet()) {
						renewalDataRecToModify.updateFieldByName(currChange.getKey(), currChange.getValue());
					}
				}

				setProgress((int) (++changesApplied / (totalChangesToApplyCount * 0.01)));
				
			}
		}
		
		setProgress(100);
		return null;
	}

	/**
	 * @return the renewalData
	 */
	public Map<Integer, RenewalData> getRenewalData() {
		return renewalData;
	}

}
