package de.infinigate.renewalTool.dataEnricher.v2.workers;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JOptionPane;
import javax.swing.SwingWorker;

import de.infinigate.renewalTool.dataEnricher.v2.common.SQLErrorHandler;
import de.infinigate.renewalTool.dataEnricher.v2.common.XmlConfigManager;
import de.infinigate.renewalTool.dataEnricher.v2.data.RenewalData;

public class RenewalDataObtainer extends SwingWorker<Void,Void> {

	/**
	 * The RenewalData that was obtained from the DB (after doInBackground() was called).
	 */
	private RenewalData[] output = null;
	
	/**
	 * The RenewalData recs obtained accessible via their respective IDs.
	 */
	private Map<Integer, RenewalData> outputIndexed = null;
	
	/**
	 * The xmlConfig to obtain DB-info.
	 */
	XmlConfigManager xmlConfig;
	
	/**
	 * The "Profile Code" of the Renewal Import Profile whose data must be obtained.
	 */
	String renewalImportProfileCode;
	
	/**
	 * Standard CTor for a RenewalDataObtainer.
	 * @param xmlConfig the xmlConfig containing information on how to connect to the DB.
	 */
	public RenewalDataObtainer(XmlConfigManager xmlConfig, String renewalImportProfileCode) {
		this.xmlConfig = xmlConfig;
		this.renewalImportProfileCode = renewalImportProfileCode;
		
	}
	
	@Override
	protected Void doInBackground() throws Exception {

		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		} catch (ClassNotFoundException e) {
			JOptionPane.showMessageDialog(null, "Error loading SQL-Driver!", "Error", JOptionPane.ERROR_MESSAGE);
			System.exit(1);
		}
		
		String connectionUrl = "jdbc:sqlserver://" + xmlConfig.getSqlAddress() + ";integratedSecurity=true";
		Connection con = null;
		try {
			con = DriverManager.getConnection(connectionUrl);
		} catch (SQLException e) {
			SQLErrorHandler.makeSQLExceptionUserReadable(e, "Error connecting to SQL-DB!");
			System.exit(1);
		}
		
		ResultSet results = null;
		try {
			Statement stmt = con.createStatement();
			results = stmt.executeQuery("SELECT [Import Profile Code] ,[ID] ,[Reseller Name], [Your Reference], [EndUser Name], "
					+ "[EndUser Post Code], [EndUser City], [EndUser Address], [Licensed Users], [Expiration Date], [Item No_], "
					+ "[Alt_ Item No_], [Sales Order No_], [Description 1], [Description 2], [Description 3], [Description 4], "
					+ "[Serial No_], [Device Type], [Subscription Type], [Salesperson Ident_ Code], [E-Mail TO], [Salutation], "
					+ "[Renewal Status], [Profile Special Field 1], [Profile Special Field 2], [Profile Special Field 3], "
					+ "[NAV Customer No_], [NAV Contact no_], [EndUser Company], [E-Mail CC], [E-Mail BCC], [Overall Notification], "
					+ "[Overwrite E-Mail Body] FROM [" + xmlConfig.getNavDbName() + "].dbo.[" +	xmlConfig.getNavCompanyName() 
					+ "$_Renewal Data] WHERE [Import Profile Code] = '" + renewalImportProfileCode + "'");
		} catch (SQLException e) {
			SQLErrorHandler.makeSQLExceptionUserReadable(e, "Error creating SQL-Statement to obtain RenewalData from DB!");
			System.exit(1);
		}
		
		ArrayList<RenewalData> renewalData = new ArrayList<RenewalData>();
		Map<Integer, RenewalData> renewalDataIndexed = new HashMap<Integer, RenewalData>();
		
		try {
			while (results.next()) {
				RenewalData currNewRec = createRenewalRecFromResultSet(results);
				renewalData.add(currNewRec);
				renewalDataIndexed.put(currNewRec.getId(), currNewRec);
			}

		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Error reading results from DB! Message:\n" + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
			System.exit(1);
		}
		
		if (renewalData.size() > 0) {
			output = renewalData.toArray(new RenewalData[1]);
			outputIndexed = renewalDataIndexed;
		} else {
			JOptionPane.showMessageDialog(null, "There could no RenewalData records be found in the database!\nPlease make sure there are some present, "
					+ "otherwise there is nothing to enrich.\nIf you are sure that there should be records present for the selected "
					+ "\"Import Profile Code\",\nconsider contacting your admin for further assistance.\n\nThe DataEnricher will terminate now.", "Error", JOptionPane.ERROR_MESSAGE);
			System.exit(1);			
		}

		return null;
	}
	
	/**
	 * Helper method to create a new RenewalRec from a ResultSet.
	 * @param results the ResultSetToUse
	 * @return a new renewal rec
	 * @throws SQLException in case anything goes wrong accessing the ResultSet
	 */
	private RenewalData createRenewalRecFromResultSet(ResultSet results) throws SQLException {
		return new RenewalData(results.getString(RenewalData.PROFILE_CODE_COLNAME),
				results.getInt(RenewalData.ID_COLNAME),
				results.getString(RenewalData.RESELLER_NAME_COLNAME),
				results.getString(RenewalData.CUSTOMER_NO_COLNAME),
				results.getString(RenewalData.YOUR_REFERENCE_COLNAME),
				results.getString(RenewalData.END_USER_NAME_COLNAME),
				results.getString(RenewalData.END_USER_COMPANY_COLNAME),
				results.getString(RenewalData.END_USER_POST_CODE_COLNAME),
				results.getString(RenewalData.END_USER_CITY_COLNAME),
				results.getString(RenewalData.END_USER_ADDRESS_COLNAME),
				results.getInt(RenewalData.LICENSED_USERS_COLNAME),
				results.getDate(RenewalData.EXPIRATION_DATE_COLNAME),
				results.getString(RenewalData.ITEM_NO_COLNAME),
				results.getString(RenewalData.ALT_ITEM_NO_COLNAME),
				results.getString(RenewalData.SALES_ORDER_NO_COLNAME),
				results.getString(RenewalData.DESCRIPTION_1_COLNAME),
				results.getString(RenewalData.DESCRIPTION_2_COLNAME),
				results.getString(RenewalData.DESCRIPTION_3_COLNAME),
				results.getString(RenewalData.DESCRIPTION_4_COLNAME),
				results.getString(RenewalData.SERIAL_NO_COLNAME),
				results.getString(RenewalData.DEVICE_TYPE_COLNAME),
				results.getString(RenewalData.SUBSCRIPTION_TYPE_COLNAME),
				results.getString(RenewalData.SALESPERSON_IDENT_CODE_COLNAME),
				results.getString(RenewalData.E_MAIL_TO_COLNAME),
				results.getString(RenewalData.SALUTATION_COLNAME),
				results.getString(RenewalData.RENEWAL_STATUS_COLNAME),
				results.getString(RenewalData.PROFILE_SPECIAL_FIELD_1_COLNAME),
				results.getString(RenewalData.PROFILE_SPECIAL_FIELD_2_COLNAME),
				results.getString(RenewalData.PROFILE_SPECIAL_FIELD_3_COLNAME),
				results.getString(RenewalData.NAV_CONTACT_NO_COLNAME), 
				results.getString(RenewalData.OVERWRITE_EMAIL_BODY_COLNAME),
				results.getBoolean(RenewalData.OVERALL_NOTIFICATION_COLNAME),
				null
		);
	}

	/**
	 * @return the output
	 */
	public RenewalData[] getOutput() {
		return output;
	}

	/**
	 * @return the outputIndexed
	 */
	public Map<Integer, RenewalData> getOutputIndexed() {
		return outputIndexed;
	}


}
