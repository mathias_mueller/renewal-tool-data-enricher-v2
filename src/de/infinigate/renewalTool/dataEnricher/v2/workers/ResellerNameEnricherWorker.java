package de.infinigate.renewalTool.dataEnricher.v2.workers;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.swing.JOptionPane;

import de.infinigate.renewalTool.dataEnricher.v2.common.SQLErrorHandler;
import de.infinigate.renewalTool.dataEnricher.v2.data.RenewalData;

/**
 * Class that tries to enrich RenewalData with "Reseller Name"s by looking for their "Serial No."s.
 * @author mathias.mueller
 */
public class ResellerNameEnricherWorker extends AbstractDataEnricherWorker<Void, Void> {
	
	public static final String SETTINGS_SAVE_NAME = "resellerNameEnrichmentData";
	
	public ResellerNameEnricherWorker() {
		super("Searching for \"Reseller Name\"-values");
	}
	
	@Override
	protected Void doInBackground() {

		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		} catch (ClassNotFoundException e) {
			JOptionPane.showMessageDialog(null, "Error loading SQL-Driver!", "Error", JOptionPane.ERROR_MESSAGE);
			System.exit(1);
		}
		
		String connectionUrl = "jdbc:sqlserver://" + getXmlConfig().getSqlAddress() + ";integratedSecurity=true";
		Connection con = null;
		try {
			con = DriverManager.getConnection(connectionUrl);
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Error connecting to SQL-DB! Message:\n" + e.getMessage(), 
					"Error", JOptionPane.ERROR_MESSAGE);
			System.exit(1);
		}
		
		// check which RenewalData recs are in need of and able to be enriched.
		ArrayList<RenewalData> recsToCheck = new ArrayList<RenewalData>();
		for (RenewalData currRec : getInputData()) {
			if (currRec.getSerialNo() != null && !currRec.getSerialNo().equals(""))
				recsToCheck.add(currRec);
		}
		int totalRecCount = recsToCheck.size();
		int recsProcessed = 0;
		
		if (totalRecCount > 0) {
		
			PreparedStatement selII = null;
			try {
				selII = con.prepareStatement("SELECT [Sell-to Customer Name]"
						+ "FROM [" + getXmlConfig().getNavDbName() + "].[dbo].[" + getXmlConfig().getNavCompanyName() + "$_Installed Item]"
						+ "WHERE [Serial No_] = ?");
			} catch (SQLException e) {
				SQLErrorHandler.makeSQLExceptionUserReadable(e, "Error preparing statement to obtain information from "
						+ "\"Installed Item\" table");
				System.exit(1);
			}
			for (RenewalData currRecToCheck : recsToCheck) {
				ResultSet results = null;
				try {
					selII.clearParameters();
					selII.setString(1, currRecToCheck.getSerialNo());
					results = selII.executeQuery();
				} catch (SQLException e) {
					SQLErrorHandler.makeSQLExceptionUserReadable(e, "Error executing statement to obtain"
							+ " information from \"Installed Item\" table.");
					System.exit(1);
				}
				try {
					ArrayList<Map<String,String>> retArrayList = new ArrayList<Map<String,String>>();
					while (results.next()) {
						Map<String,String> retMap = new HashMap<String,String>();
						retMap.put(RenewalData.RESELLER_NAME_COLNAME, results.getString(1));
						retArrayList.add(retMap);
					}
					if (retArrayList.size() > 0) {
						if (oneValueDifferentFromCurrentInRec(currRecToCheck, retArrayList))
							output.put(currRecToCheck.getId(), retArrayList);
					}
				} catch (SQLException e) {
					SQLErrorHandler.makeSQLExceptionUserReadable(e, "Error executing statement to obtain"
							+ " information from \"Installed Item\" table.");
					System.exit(1);
				}

				setProgress((int) (++recsProcessed / (totalRecCount * 0.01)));
			}

		} else {
			setProgress(100);
			appendTextToTextArea("No eligible records were found! (There need to be records without \"Reseller Name\" but with a "
					+ "\"Serial No.\" entered in order to do this enrichment step.", 1);
		}
		return null;
	}

	@Override
	public String getSettingsSaveName() {
		return SETTINGS_SAVE_NAME;
	}

}
