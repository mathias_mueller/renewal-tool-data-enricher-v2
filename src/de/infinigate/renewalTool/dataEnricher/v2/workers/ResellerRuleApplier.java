package de.infinigate.renewalTool.dataEnricher.v2.workers;

import java.util.Collection;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.swing.JTextArea;
import javax.swing.SwingWorker;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;

import de.infinigate.renewalTool.dataEnricher.v2.data.RenewalData;
import de.infinigate.renewalTool.dataEnricher.v2.data.ResellerRuleData;

public class ResellerRuleApplier extends SwingWorker<Void, Void> {
	
	/**
	 * Input data, Map accessible by IDs.
	 */
	private Map<Integer, RenewalData> data;
	
	/**
	 * The Multimap containing the data on which the config shall be applied.
	 */
	private Multimap<String, RenewalData> dataByResellerName;
	
	/**
	 * A map containing the ResellerSpecConfigData.
	 */
	private Map<String, ResellerRuleData> config;
	
	/**
	 * Reference to parent window's textArea for display of status information.
	 */
	private JTextArea statusTextArea;
	
	/**
	 * Standard CTor for a ResellerSpecConfigApplier.
	 */
	public ResellerRuleApplier() {
		
	}
	
	/**
	 * Method that does the actual work.
	 * Use setters for configData and input before calling
	 */
	@Override
	protected Void doInBackground() throws Exception {

		if (data != null && config != null) {

			buildDataByResellerNameFromIndexed();
			
			int totalConfigRecCount = config.size();
			int processedConfigRecsCount = 0;
			
			// parse for wildcard '*'
			Set<String> dataKeySet = dataByResellerName.keySet();
			Multimap<String, String> wildcardMatches = ArrayListMultimap.create();
			for (Entry<String, ResellerRuleData> currConfigEntry : config.entrySet()) {
				if (currConfigEntry.getKey().contains("*")) {
					String currConfigKey = currConfigEntry.getKey();
					for (String currDataKey : dataKeySet) {
						if (currDataKey.matches(currConfigKey.replaceAll("[*]",".*"))) {
							wildcardMatches.put(currConfigKey, currDataKey);
						}
					}
				}
			}
			
			for (Entry<String, ResellerRuleData> currConfigEntry : config.entrySet()) {
				if (currConfigEntry.getKey().contains("*")) {
					Collection<String> renewalKeysToModify = wildcardMatches.get(currConfigEntry.getKey());
					for (String currKeyDataToModify : renewalKeysToModify) {
						Collection<RenewalData> renewalDataForConfigKey = dataByResellerName.get(currKeyDataToModify); 
						if (renewalDataForConfigKey != null && renewalDataForConfigKey.size() > 0) {
							ResellerRuleData currConfig = currConfigEntry.getValue();
							applyConfig(renewalDataForConfigKey, currConfig);
						}
					}
				} else {
					Collection<RenewalData> renewalDataForConfigKey = dataByResellerName.get(currConfigEntry.getKey()); 
					if (renewalDataForConfigKey != null && renewalDataForConfigKey.size() > 0) {
						ResellerRuleData currConfig = currConfigEntry.getValue();
						applyConfig(renewalDataForConfigKey, currConfig);
					}
				}
				
				int progress = ++processedConfigRecsCount/totalConfigRecCount*100;
				if (progress < 0)
					progress = 0;
				if (progress > 100)
					progress = 100;
				setProgress(progress);				
			}
		} else {
			if (statusTextArea != null) {
				if (data != null) {
					statusTextArea.append("\nNo data was found to apply config to.");
				} 
				if (config != null) {
					statusTextArea.append("\nNo reseller spec. config. data was found when trying to apply it.");
				}
			}
		}
		
		return null;
	}

	/**
	 * Converts the regular format of the RenewalData (indexed by "Id") to the one used in this class, Mulitmap<"Reseller Name", (all RenewalData for Kex)
	 */
	private void buildDataByResellerNameFromIndexed() {

		Multimap<String, RenewalData> dataByResellerNameUnderConstr = HashMultimap.create();
		
		for (Entry<Integer, RenewalData> currEntry : data.entrySet()) {
			RenewalData currRec = currEntry.getValue();
			dataByResellerNameUnderConstr.put(currRec.getResellerName(), currRec);
		}
		
		dataByResellerName = dataByResellerNameUnderConstr;
		
	}

	/**
	 * Applies given RenewalData to the Collection of RenewalData.
	 * @param renewalDataToModify the data to apply the config to
	 * @param configToApply the config to apply
	 */
	private void applyConfig(Collection<RenewalData> renewalDataToModify,
			ResellerRuleData configToApply) {
		for (RenewalData currRenewalDataDataset : renewalDataToModify) {
			if (configToApply.getOverwriteTo() != null && configToApply.getOverwriteTo().length() > 2) 
				data.get(currRenewalDataDataset.getId()).seteMailTo(configToApply.getOverwriteTo());
			if (configToApply.getOverwriteCc() != null && configToApply.getOverwriteCc().length() > 2) 
				data.get(currRenewalDataDataset.getId()).seteMailCc(configToApply.getOverwriteCc());
			if (configToApply.getOverwriteBcc() != null && configToApply.getOverwriteBcc().length() > 2) 
				data.get(currRenewalDataDataset.getId()).seteMailBcc(configToApply.getOverwriteBcc());
			if (configToApply.getOverwriteSalutation() != null && !configToApply.getOverwriteSalutation().equals(""))
				data.get(currRenewalDataDataset.getId()).setSalutation(configToApply.getOverwriteSalutation());
			if (configToApply.getOverwriteBody() != null && configToApply.getOverwriteBody().length() > 1)
				data.get(currRenewalDataDataset.getId()).setOverwriteEmailBody(configToApply.getOverwriteBody());
			if (!configToApply.isNotificationTypeOverall()) {
				data.get(currRenewalDataDataset.getId()).setOverallNotification(false);
			}
		}
	}

	/**
	 * Setter for the input data on which the config data is to be applied.
	 * Does not need ot be set manually, can be genereated by calling function buildDataByResellerNameFromIndexed
	 * @param data the data
	 */
	public void setDataByResellerName(Multimap<String, RenewalData> data) {
		this.dataByResellerName = data;
	}
	
	/**
	 * Getter for the data that is/was to be modified.
	 * @return the data
	 */
	public Multimap<String, RenewalData> getDataByResellerName() {
		return dataByResellerName;
	}
	
	/**
	 * Setter for the data to modify by Integer-index.
	 * @param data
	 */
	public void setData(Map<Integer, RenewalData> data) {
		this.data = data;
	}

	/**
	 * @return the data
	 */
	public Map<Integer, RenewalData> getData() {
		return data;
	}

	/**
	 * Setter for the config data that shall be applied.
	 * @param config the config data
	 */
	public void setConfig(Map<String, ResellerRuleData> config) {
		this.config = config;
	}

	/**
	 * Setter for the JTextArea used to display status information.
	 * @param textArea the JTextArea
	 */
	public void setStatusTextArea(JTextArea textArea) {
		statusTextArea = textArea;
	}

}
