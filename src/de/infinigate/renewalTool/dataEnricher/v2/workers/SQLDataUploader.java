package de.infinigate.renewalTool.dataEnricher.v2.workers;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Map;

import javax.swing.JOptionPane;
import javax.swing.SwingWorker;

import de.infinigate.renewalTool.dataEnricher.v2.common.SQLErrorHandler;
import de.infinigate.renewalTool.dataEnricher.v2.common.XmlConfigManager;
import de.infinigate.renewalTool.dataEnricher.v2.data.RenewalData;

/**
 * Class used to re-upload the modified data. 
 * @author Mathias.mueller
 */
public class SQLDataUploader extends SwingWorker<Void, Void> {

	/**
	 * The data that must be uploaded.
	 * Needs to be set
	 */
	private Map<Integer, RenewalData> dataToUpload;
	
	/**
	 * Used to obtain DB-information.
	 */
	private XmlConfigManager xmlConfig;

	/**
	 * Counter for how many recs have been updated.
	 * (this means they have actually been changed)
	 */
	private int recsUpdated;

	/**
	 * Counter for how many recs have been processed.
	 * (NOT updated, only processed)
	 */
	private int recsProcessed;
	
	/**
	 * Builds a SQLDateUploader.
	 * @param xmlConfig to obtain DB-information
	 */
	public SQLDataUploader(XmlConfigManager xmlConfig) {
		this.xmlConfig = xmlConfig;
	}
	
	/**
	 * Method that does the actual work in this class.
	 */
	@Override
	protected Void doInBackground() throws Exception {
		
		setProgress(0);
		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		} catch (ClassNotFoundException e1) {
			JOptionPane.showMessageDialog(null, "Error loading SQL-Driver!", "Error", JOptionPane.ERROR_MESSAGE);
			System.exit(1);
		}
		
		String connectionUrl = "jdbc:sqlserver://" + xmlConfig.getSqlAddress() + ";integratedSecurity=true";
		Connection con = null;
		try {
			con = DriverManager.getConnection(connectionUrl);
		} catch (SQLException e2) {
			SQLErrorHandler.makeSQLExceptionUserReadable(e2, "Error connecting to DB!");
		}

		String stmt =
				"UPDATE ["+ xmlConfig.getNavDbName() + "].[dbo].[" + xmlConfig.getNavCompanyName() + "$_Renewal Data] "
						+ "SET [" + RenewalData.PROFILE_CODE_COLNAME + "] = ?"
						+ ",[" + RenewalData.ID_COLNAME + "] = ?"
						+ ",[" + RenewalData.RESELLER_NAME_COLNAME + "] = ?"
						+ ",[" + RenewalData.YOUR_REFERENCE_COLNAME + "] = ?"
						+ ",[" + RenewalData.END_USER_NAME_COLNAME + "] = ?"
						+ ",[" + RenewalData.END_USER_POST_CODE_COLNAME + "] = ?"
						+ ",[" + RenewalData.END_USER_CITY_COLNAME + "] = ?"
						+ ",[" + RenewalData.END_USER_ADDRESS_COLNAME + "] = ?"
						+ ",[" + RenewalData.LICENSED_USERS_COLNAME + "] = ?"
						+ ",[" + RenewalData.EXPIRATION_DATE_COLNAME + "] = ?"
						+ ",[" + RenewalData.ITEM_NO_COLNAME + "] = ?"
						+ ",[" + RenewalData.ALT_ITEM_NO_COLNAME + "] = ?"
						+ ",[" + RenewalData.SALES_ORDER_NO_COLNAME + "] = ?"
						+ ",[" + RenewalData.DESCRIPTION_1_COLNAME + "] = ?"
						+ ",[" + RenewalData.DESCRIPTION_2_COLNAME + "] = ?"
						+ ",[" + RenewalData.DESCRIPTION_3_COLNAME + "] = ?"
						+ ",[" + RenewalData.DESCRIPTION_4_COLNAME + "] = ?"
						+ ",[" + RenewalData.SERIAL_NO_COLNAME + "] = ?"
						+ ",[" + RenewalData.DEVICE_TYPE_COLNAME + "] = ?"
						+ ",[" + RenewalData.SUBSCRIPTION_TYPE_COLNAME + "] = ?"
						+ ",[" + RenewalData.SALESPERSON_IDENT_CODE_COLNAME + "] = ?"
						+ ",[" + RenewalData.E_MAIL_TO_COLNAME + "] = ?"
						+ ",[" + RenewalData.SALUTATION_COLNAME + "] = ?"
						+ ",[" + RenewalData.RENEWAL_STATUS_COLNAME + "] = ?"
						+ ",[" + RenewalData.PROFILE_SPECIAL_FIELD_1_COLNAME + "] = ?"
						+ ",[" + RenewalData.PROFILE_SPECIAL_FIELD_2_COLNAME + "] = ?"
						+ ",[" + RenewalData.PROFILE_SPECIAL_FIELD_3_COLNAME + "] = ?"
						+ ",[" + RenewalData.CUSTOMER_NO_COLNAME + "] = ?"
						+ ",[" + RenewalData.NAV_CONTACT_NO_COLNAME + "] = ?"
						+ ",[" + RenewalData.END_USER_COMPANY_COLNAME + "] = ?"
						+ ",[" + RenewalData.E_MAIL_CC_COLNAME + "] = ?"
						+ ",[" + RenewalData.E_MAIL_BCC_COLNAME + "] = ?"
						+ ",[" + RenewalData.OVERALL_NOTIFICATION_COLNAME + "] = ?"
						+ ",[" + RenewalData.OVERWRITE_EMAIL_BODY_COLNAME + "] = ? "
						+ "WHERE [" + RenewalData.PROFILE_CODE_COLNAME + "] = ? AND [" + RenewalData.ID_COLNAME + "] = ?";
		
		PreparedStatement updateStmt = null;
		try {
			updateStmt = con.prepareStatement(stmt);
		} catch (SQLException e1) {
			SQLErrorHandler.makeSQLExceptionUserReadable(e1, "Error creating statement to update data after enrichments!");
			System.exit(1);
		}
		Collection<RenewalData> renewalDataColl = dataToUpload.values();
		int totalRecsToUpload = renewalDataColl.size();
		recsProcessed = 0;
		recsUpdated = 0;
		
		for (RenewalData currRenewalData : renewalDataColl) {
			try {
				if (currRenewalData.isChanged()) {
					fillStatement(updateStmt, currRenewalData);
					updateStmt.execute();
					recsUpdated++;
				}
				setProgress((int) (++recsProcessed / (totalRecsToUpload * 0.01)));
			} catch (SQLException e1) {
				SQLErrorHandler.makeSQLExceptionUserReadable(e1, "Error executing statement to update data after enrichments!");
				System.exit(1);
			}
		}
		
		return null;
	}

	/**
	 * Sets all parameters for the update statement.
	 * @param updateStmt the statement to fill
	 * @param currRenewalData the data to fill the statement with
	 * @throws SQLException in case anything goes wrong
	 */
	private void fillStatement(PreparedStatement updateStmt,
			RenewalData currRenewalData) throws SQLException {
		
		updateStmt.clearParameters();
		
		// Values
		updateStmt.setString(1, getValOrEmptyString(currRenewalData.getProfileCode()));
		updateStmt.setInt(2, currRenewalData.getId());
		updateStmt.setString(3, getValOrEmptyString(currRenewalData.getResellerName()));
		updateStmt.setString(4, getValOrEmptyString(currRenewalData.getYourReference()));
		updateStmt.setString(5, getValOrEmptyString(currRenewalData.getEndUserName()));
		updateStmt.setString(6, getValOrEmptyString(currRenewalData.getEndUserPostCode()));
		updateStmt.setString(7, getValOrEmptyString(currRenewalData.getEndUserCity()));
		updateStmt.setString(8, getValOrEmptyString(currRenewalData.getEndUserAddress()));
		updateStmt.setInt(9, currRenewalData.getLicensedUsers());
		updateStmt.setDate(10, currRenewalData.getExpirationDate());
		updateStmt.setString(11, getValOrEmptyString(currRenewalData.getItemNo()));
		updateStmt.setString(12, getValOrEmptyString(currRenewalData.getAltItemNo()));
		updateStmt.setString(13, getValOrEmptyString(currRenewalData.getSalesOrderNo()));
		updateStmt.setString(14, getValOrEmptyString(currRenewalData.getDescription1()));
		updateStmt.setString(15, getValOrEmptyString(currRenewalData.getDescription2()));
		updateStmt.setString(16, getValOrEmptyString(currRenewalData.getDescription3()));
		updateStmt.setString(17, getValOrEmptyString(currRenewalData.getDescription4()));
		updateStmt.setString(18, getValOrEmptyString(currRenewalData.getSerialNo()));
		updateStmt.setString(19, getValOrEmptyString(currRenewalData.getDeviceType()));
		updateStmt.setString(20, getValOrEmptyString(currRenewalData.getSubscriptionType()));
		updateStmt.setString(21, getValOrEmptyString(currRenewalData.getSalespersonIdentCode()));
		updateStmt.setString(22, getValOrEmptyString(currRenewalData.geteMailTo()));
		updateStmt.setString(23, getValOrEmptyString(currRenewalData.getSalutation()));
		updateStmt.setString(24, getValOrEmptyString(currRenewalData.getRenewalStatus()));
		updateStmt.setString(25, getValOrEmptyString(currRenewalData.getProfileSpecialField1()));
		updateStmt.setString(26, getValOrEmptyString(currRenewalData.getProfileSpecialField2()));
		updateStmt.setString(27, getValOrEmptyString(currRenewalData.getProfileSpecialField3()));
		updateStmt.setString(28, getValOrEmptyString(currRenewalData.getCustomerNo()));
		updateStmt.setString(29, getValOrEmptyString(currRenewalData.getContactNo()));
		updateStmt.setString(30, getValOrEmptyString(currRenewalData.getEndUserCompany()));
		updateStmt.setString(31, getValOrEmptyString(currRenewalData.geteMailCc()));
		updateStmt.setString(32, getValOrEmptyString(currRenewalData.geteMailBcc()));
		updateStmt.setBoolean(33, currRenewalData.isOverallNotification());
		updateStmt.setString(34, getValOrEmptyString(currRenewalData.getOverwriteEmailBody()));
		
		// Filters
		updateStmt.setString(35, currRenewalData.getProfileCode());
		updateStmt.setInt(36, currRenewalData.getId());
		
	}

	/**
	 * Helper method. 
	 * Returns given String (if not empty) or ""
	 * @param stringToTest the String that should be tested
	 * @return the string or if its empty ""
	 */
	private String getValOrEmptyString(String stringToTest) {
		if (stringToTest == null)
			return "";
		else 
			return stringToTest;
	}

	/**
	 * @return the dataToUpload
	 */
	public Map<Integer, RenewalData> getDataToUpload() {
		return dataToUpload;
	}

	/**
	 * @param dataToUpload the dataToUpload to set
	 */
	public void setDataToUpload(Map<Integer, RenewalData> dataToUpload) {
		this.dataToUpload = dataToUpload;
	}

	/**
	 * @return the recsUpdated
	 */
	public int getRecsUpdated() {
		return recsUpdated;
	}

	/**
	 * @param recsUpdated the recsUpdated to set
	 */
	public void setRecsUpdated(int recsUpdated) {
		this.recsUpdated = recsUpdated;
	}

	/**
	 * @return the recsProcessed
	 */
	public int getRecsProcessed() {
		return recsProcessed;
	}

	/**
	 * @param recsProcessed the recsProcessed to set
	 */
	public void setRecsProcessed(int recsProcessed) {
		this.recsProcessed = recsProcessed;
	}
	
}
