package de.infinigate.renewalTool.dataEnricher.v2.workers;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Map;
import java.util.Map.Entry;

import javax.swing.JOptionPane;
import javax.swing.SwingWorker;

import de.infinigate.renewalTool.dataEnricher.v2.common.SQLErrorHandler;
import de.infinigate.renewalTool.dataEnricher.v2.common.XmlConfigManager;
import de.infinigate.renewalTool.dataEnricher.v2.data.ResellerRuleData;


/**
 * Class ment to upload ResellerRules into the SQL-DB.
 * @author mathias.mueller
 */
public class SQLResellerRuleUploader extends SwingWorker<Void, Void> {
	
	/**
	 * Reference to this programs xmlConfigManager.
	 */
	private XmlConfigManager xmlConfig;
	
	/**
	 * The rules that are to be uploaded.
	 */
	private Map<String, ResellerRuleData> rulesToUpload;
	
	/**
	 * The name/code of the import profile.
	 */
	private String importProfileCode;
	
	/**
	 * Method that does the actual work for this class.
	 */
	public Void doInBackground() throws Exception {
		
		setProgress(0);
		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		} catch (ClassNotFoundException e1) {
			JOptionPane.showMessageDialog(null, "Error loading SQL-Driver!", "Error", JOptionPane.ERROR_MESSAGE);
			System.exit(1);
		}
		
		String connectionUrl = "jdbc:sqlserver://" + xmlConfig.getSqlAddress() + ";integratedSecurity=true";
		Connection con = null;
		try {
			con = DriverManager.getConnection(connectionUrl);
		} catch (SQLException e2) {
			SQLErrorHandler.makeSQLExceptionUserReadable(e2, "Error connecting to DB!");
		}
		
		PreparedStatement upsert = null;
		try {
			upsert = con.prepareStatement("UPDATE [" + xmlConfig.getNavDbName() + "].[dbo].[" + xmlConfig.getNavCompanyName() + "$_Renewal Reseller Rules]" 
					+ "SET [Import Profile Code] = ?,"
					+ "	[Reseller Name] = ?,"
					+ "	[Overwrite To] = ?,"
					+ "	[Overwrite Cc] = ?,"
					+ "	[Overwrite Bcc] = ?,"
					+ "	[Overwrite Salutation] = ?,"
					+ "	[Overwrite Body Code] = ?,"
					+ "	[Notification Type] = ?"
					+ " WHERE [Import Profile Code] = ?"
					+ "	AND [Reseller Name] = ? "
					+ "IF @@ROWCOUNT = 0 BEGIN"
					+ "	INSERT INTO [" + xmlConfig.getNavDbName() + "].[dbo].[" + xmlConfig.getNavCompanyName() + "$_Renewal Reseller Rules] VALUES"
					+ "		(?,?,?,?,?,?,?,?)"
					+ "END");
		} catch (SQLException e) {
			SQLErrorHandler.makeSQLExceptionUserReadable(e, "Error preparing statement to insert new ResellerRules!");
			System.exit(1);
		}
		
		// prepare figures for progress
		int totalNoRulesToUpload = rulesToUpload.entrySet().size();
		int processed = 0;
		
		try {
			for (Entry<String, ResellerRuleData> currEntry : rulesToUpload.entrySet()) {
				ResellerRuleData currRule = currEntry.getValue();
				int notificationTypeAsInt = currRule.isNotificationTypeOverall() ? 0 : 1; // Not a bool anymore but an option where 0 = Overall, 1 = Particular 
				upsert.clearParameters();
				upsert.setString(1, importProfileCode);
				upsert.setString(2, currRule.getResellerName());
				upsert.setString(3, currRule.getOverwriteTo());
				upsert.setString(4, currRule.getOverwriteCc());
				upsert.setString(5, currRule.getOverwriteBcc());
				upsert.setString(6, currRule.getOverwriteSalutation());
				upsert.setString(7, currRule.getOverwriteBody());
				upsert.setInt(8, notificationTypeAsInt);
				upsert.setString(9, importProfileCode);
				upsert.setString(10, currRule.getResellerName());
				upsert.setString(11, currRule.getResellerName());
				upsert.setString(12, importProfileCode);
				upsert.setString(13, currRule.getOverwriteTo());
				upsert.setString(14, currRule.getOverwriteCc());
				upsert.setString(15, currRule.getOverwriteBcc());
				upsert.setString(16, currRule.getOverwriteSalutation());
				upsert.setInt(17, notificationTypeAsInt);
				upsert.setString(18, currRule.getOverwriteBody());
		
				upsert.execute();
				processed++;
				int progress = processed / totalNoRulesToUpload * 100;
				if (progress < 0)
					progress = 0;
				else if (progress > 100)
					progress = 100;
				setProgress(progress);
			}
		} catch (SQLException e) {
			SQLErrorHandler.makeSQLExceptionUserReadable(e, "Error executing statement to insert new ResellerRules!");
		}
			
		
		return null;
		
	}

	/**
	 * @param xmlConfig the xmlConfig to set
	 */
	public void setXmlConfig(XmlConfigManager xmlConfig) {
		this.xmlConfig = xmlConfig;
	}

	/**
	 * @param rulesToUpload the rulesToUpload to set
	 */
	public void setRulesToUpload(Map<String, ResellerRuleData> rulesToUpload) {
		this.rulesToUpload = rulesToUpload;
	}

	/**
	 * @param importProfileCode the importProfileCode to set
	 */
	public void setImportProfileCode(String importProfileCode) {
		this.importProfileCode = importProfileCode;
	}

}
